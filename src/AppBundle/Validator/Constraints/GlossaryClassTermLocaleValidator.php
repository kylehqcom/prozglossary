<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use AppBundle\Repository\TermRepository;

/**
 * @Annotation
 */
class GlossaryClassTermLocaleValidator extends ConstraintValidator
{
	protected $repository;

	public function __construct(TermRepository $repository)
	{
		$this->repository = $repository;
	}

	public function validate($termEntity, Constraint $constraint)
    {
    	if ($this->repository->hasAdditional($termEntity->getGlossary(), $termEntity->getLocale(), $termEntity)) {
            $this->context->buildViolation($constraint->message)
	            ->setParameter('{{ string }}', $termEntity->getTerm())
	            ->addViolation();
        }
    }
}
