<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class GlossaryClassTermLocale extends Constraint
{
	public $message = 'The term "{{ string }}" is already in use on this glossary with the same locale.';

	public function getTargets()
	{
		return self::CLASS_CONSTRAINT;
	}
}