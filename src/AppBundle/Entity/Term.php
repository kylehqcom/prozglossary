<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AssertLocale;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TermRepository")
 * @ORM\Table(name="proz_term", uniqueConstraints={@UniqueConstraint(name="term_idx", columns={"term", "locale", "glossary_id"})})
 *
 * @AssertLocale\GlossaryClassTermLocale
 *
 * Defines the properties of a Term entity.
 */
class Term
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Glossary
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Glossary", inversedBy="terms")
     * @ORM\JoinColumn(nullable=false)
     */
    private $glossary;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $translator;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     * @Assert\Length(min=3, minMessage="term.too_short_term")
     */
    private $term;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $locale;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="term.blank_definition")
     * @Assert\Length(min=10, minMessage="term.too_short_defintion")
     */
    private $definition;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt= new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Glossary
     */
    public function getGlossary()
    {
    	return $this->glossary;
    }

    /**
     * @param Glossary $glossary
     */
    public function setGlossary(Glossary $glossary)
    {
    	$this->glossary = $glossary;
    }

    /**
     * @return User
     */
    public function getTranslator()
    {
    	return $this->translator;
    }

    /**
     * @param User $translator
     */
    public function setTranslator(User $translator)
    {
    	$this->translator = $translator;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
    	$this->locale = $locale;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt)
    {
    	$this->createdAt = $createdAt;
    }

    public function getDefinition()
    {
    	return $this->definition;
    }

    public function setDefinition($definition)
    {
    	$this->definition = $definition;
    }

    public function getTerm()
    {
    	return $this->term;
    }

    public function setTerm($term)
    {
    	$this->term = strtolower($term);
    }
}
