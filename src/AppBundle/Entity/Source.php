<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SourceRepository")
 * @ORM\Table(name="proz_source")
 */
class Source
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $locale;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="source.blank_content")
     * @Assert\Length(min=10, minMessage="source.too_short_content")
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $createdAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @var Glossary[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="Glossary",
     *      mappedBy="source",
     *      orphanRemoval=true
     * )
     * @ORM\OrderBy({"createdAt": "DESC"})
     */
    private $glossaries;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->glossaries = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
    	$this->locale = $locale;
    }

    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt)
    {
    	$this->createdAt = $createdAt;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;
    }

    public function getGlossaries()
    {
    	return $this->glossaries;
    }

    public function addGlossary(Glossary $glossary)
    {
    	$this->glossaries->add($glossary);
    	$glossary->setSource($this);
    }

    public function removeGlossary(Glossary $glossary)
    {
    	$this->glossaries->removeElement($glossary);
    }
}
