<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GlossaryRepository")
 * @ORM\Table(name="proz_glossary", uniqueConstraints={@UniqueConstraint(name="glossary_idx", columns={"source_id", "locale"})})
 *
 * Defines the properties of a Glossary entity.
 */
class Glossary
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Source
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Source", inversedBy="glossaries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $source;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $translator;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $locale;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $createdAt;

    /**
     * @var Glossary[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="Term",
     *      mappedBy="glossary",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     * @ORM\OrderBy({"createdAt": "DESC"})
     */
    private $terms;

    public function __construct()
    {
        $this->createdAt= new \DateTime();
        $this->terms = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Source
     */
    public function getSource()
    {
    	return $this->source;
    }

    /**
     * @param Source $source
     */
    public function setSource(Source $source)
    {
    	$this->source = $source;
    }

    /**
     * @return User
     */
    public function getTranslator()
    {
    	return $this->translator;
    }

    /**
     * @param User $translator
     */
    public function setTranslator(User $translator)
    {
    	$this->translator = $translator;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
    	$this->locale = $locale;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt)
    {
    	$this->createdAt = $createdAt;
    }

    public function getTerms()
    {
    	return $this->terms;
    }

    public function addTerm(Term $term)
    {
    	$this->terms->add($term);
    	$term->setGlossary($this);
    }

    public function removeTerm(Term $term)
    {
    	$this->terms->removeElement($term);
    }
}
