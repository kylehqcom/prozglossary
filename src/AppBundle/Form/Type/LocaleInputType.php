<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Repository\LocaleRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;

class LocaleInputType extends AbstractType
{
    protected $localeRepository;

    public function __construct(LocaleRepository $repository)
    {
        $this->localeRepository = $repository;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    	$resolver->setDefault('choices', array_flip($this->localeRepository->findAll()));
    	parent::configureOptions($resolver);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
    	return ChoiceType::class;
    }
}
