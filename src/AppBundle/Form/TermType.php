<?php

namespace AppBundle\Form;

use AppBundle\Entity\Term;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\Type\LocaleInputType;

/**
 * Defines the form used to create and manipulate terms.
 */
class TermType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // For the full reference of options defined by each form field type
        // see https://symfony.com/doc/current/reference/forms/types.html
    	if ($options['display_locale']) {
    		$builder->add('locale', LocaleInputType::class, [
    				'attr' => ['autofocus' => true],
    				'disabled' => ($options['disable_locale']) ? true : false,
    				'label' => 'Locale',
    		]);
    	}

		$builder
	        ->add('term', null, [
	        		'label' => 'Term',
	        ])
            ->add('definition', null, [
                'attr' => ['autofocus' => true],
                'label' => 'Definition',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Term::class,
        	'display_locale' => true,
        	'disable_locale' => false,
        	'locale_choices' => [],
        ]);
    }
}
