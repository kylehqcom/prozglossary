<?php

namespace AppBundle\Form;

use AppBundle\Entity\Glossary;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Defines the form used to create and manipulate glossaries.
 */
class GlossaryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // For the full reference of options defined by each form field type
        // see https://symfony.com/doc/current/reference/forms/types.html
        $builder
        	->add('locale', ChoiceType::class, ['choices' => $options['locale_choices']])
            ->add('terms', GlossaryCollectionType::class, [
            		'entry_type' => TermType::class,
            		'entry_options' => ['display_locale' => $options['display_term_locale'] ? true : false]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Glossary::class,
        	'display_term_locale' => true,
        	'locale_choices' => []
        ]);
    }
}
