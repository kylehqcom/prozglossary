<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Glossary;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Defines the sample blog posts to load in the database before running the unit
 * and functional tests. Execute this command to load the data.
 *
 *   $ php bin/console doctrine:fixtures:load
 *
 * See https://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
 *
 */
class GlossaryFixtures extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
    	$glossary = new Glossary();
    	$glossary->setTranslator($this->getReference('kyle-admin'));
    	$source = $this->getReference('default-source');
    	$glossary->setSource($source);
    	$glossary->setLocale($source->getLocale());
    	$manager->persist($glossary);
    	$this->setReference('default-glossary', $glossary);
        $manager->flush();
    }

    /**
     * Instead of defining the exact order in which the fixtures files must be loaded,
     * this method defines which other fixtures this file depends on. Then, Doctrine
     * will figure out the best order to fit all the dependencies.
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
        	SourceFixtures::class,
        ];
    }
}
