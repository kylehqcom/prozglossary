<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Term;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Defines the sample blog posts to load in the database before running the unit
 * and functional tests. Execute this command to load the data.
 *
 *   $ php bin/console doctrine:fixtures:load
 *
 * See https://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
 *
 */
class TermFixtures extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
    	$glossary = $this->getReference('default-glossary');
    	$translator = $this->getReference('kyle-admin');
    	$locale = $glossary->getLocale();

    	$term = new Term();
    	$term->setTranslator($translator);
    	$term->setGlossary($glossary);
    	$term->setTerm('explode');
    	$term->setDefinition('to burst, fly into pieces, or break up violently with a loud report, as a boiler from excessive pressure of steam.');
    	$term->setLocale($locale);
    	$manager->persist($term);

    	$term = new Term();
    	$term->setTranslator($translator);
    	$term->setGlossary($glossary);
    	$term->setTerm('furnaces');
    	$term->setDefinition('a structure or apparatus in which heat may be generated, as for heating houses, smelting ores, or producing steam.');
    	$term->setLocale($locale);
    	$manager->persist($term);

    	$term = new Term();
    	$term->setTranslator($translator);
    	$term->setGlossary($glossary);
    	$term->setTerm('evolution');
    	$term->setDefinition('a pattern formed by or as if by a series of movements.');
    	$term->setLocale($locale);
    	$manager->persist($term);

    	$term = new Term();
    	$term->setTranslator($translator);
    	$term->setGlossary($glossary);
    	$term->setTerm('evolution');
    	$term->setDefinition('Un patrón formado por o como por una serie de movimientos.');
    	$term->setLocale('es');
    	$manager->persist($term);

    	$term = new Term();
    	$term->setTranslator($translator);
    	$term->setGlossary($glossary);
    	$term->setTerm('evolution');
    	$term->setDefinition('Wzór utworzony przez lub jakby przez szereg ruchów.');
    	$term->setLocale('pl');
    	$manager->persist($term);

        $manager->flush();
    }

    /**
     * Instead of defining the exact order in which the fixtures files must be loaded,
     * this method defines which other fixtures this file depends on. Then, Doctrine
     * will figure out the best order to fit all the dependencies.
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
        	GlossaryFixtures::class,
        ];
    }
}
