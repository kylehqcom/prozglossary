<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Source;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Defines the sample blog posts to load in the database before running the unit
 * and functional tests. Execute this command to load the data.
 *
 *   $ php bin/console doctrine:fixtures:load
 *
 * See https://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
 *
 */
class SourceFixtures extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const DEFAULT_SOURCE_CONTENT = 'You couldn\'t be here if stars hadn’t exploded, because the elements - the
		carbon, nitrogen, oxygen, iron, all the things that matter for evolution and for life — weren\'t
		created at the beginning of time. They were created in the nuclear furnaces of stars, and the only
		way for them to get into your body is if those stars were kind enough to explode.';

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
    	$source = new Source();
    	$source->setAuthor($this->getReference('kyle-admin'));
    	$source->setContent(self::DEFAULT_SOURCE_CONTENT);

    	$localeRepo = $this->container->get('app.locale_repository');
    	$source->setLocale(key($localeRepo->defaultLocale()));

    	$manager->persist($source);
    	$this->addReference('default-source', $source);

        $manager->flush();
    }

    /**
     * Instead of defining the exact order in which the fixtures files must be loaded,
     * this method defines which other fixtures this file depends on. Then, Doctrine
     * will figure out the best order to fit all the dependencies.
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
