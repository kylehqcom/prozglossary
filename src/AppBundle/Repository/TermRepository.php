<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Term;
use AppBundle\Entity\Glossary;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for term information.
 */
class TermRepository extends EntityRepository
{
	/**
	 * @param Glossary $glossary
	 * @param string $locale
	 * @param Term $term
	 * @return boolean
	 */
	public function hasAdditional(Glossary $glossary, $locale, Term $term)
	{
		$builder = $this->getEntityManager()
			->createQueryBuilder()
			->from(Term::class, 't')
			->select('COUNT(t.id) AS hit')
			->where('t.glossary = :glossary')
			->andWhere('t.locale = :locale')
			->andWhere('t.term = :term')
			->setParameters(['glossary' => $glossary, 'locale' => $locale, 'term' => $term->getTerm()])
		;

		if ($notId = $term->getId()) {
			$builder->andWhere('t.id != :notId')->setParameter('notId', $notId);
		}

		return (bool) $builder->getQuery()->execute([], AbstractQuery::HYDRATE_SINGLE_SCALAR);
	}

	/**
	 * @return Query
	 */
	public function filterChoiceQuery()
	{
		return $this->getEntityManager()
			->createQueryBuilder()
			->from(Term::class, 't')
			->distinct()
			->select('t.term, t.createdAt')
			->orderBy('t.createdAt', 'DESC')
			->setMaxResults(10)
			->getQuery();
	}

	/**
	 * // TODO: Create custom hydrator to return key values.
	 *
	 * @return array
	 */
	public function getFilterChoices()
	{
		$choices = [];
		$results = $this->filterChoiceQuery()->execute([], AbstractQuery::HYDRATE_ARRAY);
		foreach($results as $result) {
			$choices[$result['term']] = $result['term'];
		}

		return $choices;
	}
}
