<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for source information.
 */
class SourceRepository extends EntityRepository
{
}
