<?php

namespace AppBundle\Repository;

/**
 * This custom repository contains some fixture locales, no need to store to the database
 * as this is a test/play application.
 */
class LocaleRepository
{
	protected static $defaultLocale = ['en' => 'English'];

	protected static $locales = [
			'en' => 'English',
			'es' => 'Espanyol',
			'pl' => 'Polski'
	];

	public function defaultLocale()
	{
		return self::$defaultLocale;
	}

	public function findAll()
	{
		return self::$locales;
	}

	public function find($local)
	{
		if (isset(self::$locales[$local])) {
			return [$local => self::$locales[$local]];
		}

		return $this->defaultLocale();
	}
}
