<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use AppBundle\Entity\Glossary;
use Doctrine\ORM\QueryBuilder;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for glossaries.
 */
class GlossaryRepository extends EntityRepository
{
	const NUM_ITEMS = 50;

	/**
	 * @param string $translatorId
	 * @return QueryBuilder
	 */
	public function byTranslatorQueryBuilder($translatorId) {
		return $this->getEntityManager()
			->createQueryBuilder()
			->select('g')
			->from(Glossary::class, 'g')
			->orderBy('g.createdAt')
			->where('g.translator = :id')->setParameter('id', $translatorId)
		;
	}

	/**
	 * @param string $translatorId
	 * @param integer $hydrationMode
	 * @return mixed|\Doctrine\DBAL\Driver\Statement|array|NULL
	 */
	public function byTranslator($translatorId, $hydrationMode = null)
	{
		return $this->byTranslatorQueryBuilder($translatorId)->getQuery()->execute([], $hydrationMode);
	}

	/**
	 * @param string $translatorId
	 * @param string $sourceId
	 * @param integer $hydrationMode
	 * @return mixed|\Doctrine\DBAL\Driver\Statement|array|NULL
	 */
	public function byTranslatorAndSource($translatorId, $sourceId, $hydrationMode = null)
	{
		return $this->byTranslatorQueryBuilder($translatorId)
			->andWhere('g.source = :sourceId')
			->setParameter('sourceId', $sourceId)
			->getQuery()
			->execute([], $hydrationMode);
	}

    /**
     * @param int $page
     *
     * @return Pagerfanta
     */
	public function findLatest($page = 1, $term = null, $locale = null)
    {
		$builder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('g, t')
            ->from(Glossary::class, 'g')
            ->leftJoin('g.terms', 't')
            ->leftJoin('g.translator', 'u')
            ->orderBy('g.createdAt', 'DESC')
            ->orderBy('t.term, t.locale', 'ASC')
		;

        if ($term) {
        	$builder->andWhere('t.term = :term')->setParameter('term', $term);
        }

        if ($locale) {
        	$builder->andWhere('t.locale = :locale')->setParameter('locale', $locale);
        }

		return $this->createPaginator($builder->getQuery(), $page);
    }

    private function createPaginator(Query $query, $page)
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query, false));
        $paginator->setMaxPerPage(self::NUM_ITEMS);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}
