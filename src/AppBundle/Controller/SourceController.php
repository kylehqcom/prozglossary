<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SourceController extends Controller
{
	public function defaultSourceAction()
	{
		$source = $this->container->get('app.source_repository')->find(1);
		return $this->render(
				'default/_source.html.twig',
				array('source' => $source)
		);
	}
}
