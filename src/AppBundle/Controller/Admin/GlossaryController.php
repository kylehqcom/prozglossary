<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Term;
use AppBundle\Form\TermType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Glossary;
use AppBundle\Form\GlossaryType;

/**
 * Controller used to manage glossary contents in the backend.
 *
 * See http://knpbundles.com/keyword/admin
 *
 * @Route("/admin/glossary")
 * @Security("has_role('ROLE_ADMIN')")
 */
class GlossaryController extends Controller
{
    /**
     * Creates a new Term entity.
     *
     * @Route("/new", name="admin_glossary_new")
     * @Method({"GET", "POST"})
     *
     * NOTE: the Method annotation is optional, but it's a recommended practice
     * to constraint the HTTP methods each controller responds to (by default
     * it responds to all methods).
     */
    public function newAction(Request $request)
    {
		// Check if all glossary/locale/source combinations are complete.
		$glossaryRepo = $this->container->get('app.glossary_repository');
		$existing = $glossaryRepo->byTranslatorAndSource($this->getUser()->getId(), 1);
		$locales = $this->container->get('app.locale_repository')->findAll();
		if (count($existing) == count($locales)) {
			$this->addFlash('warning', 'No locale\'s left to add a glossary translation.');
			return $this->redirectToRoute('glossary_index');
		}

		foreach ($existing as $exist) {
			unset($locales[$exist->getLocale()]);
		}

        $glossary = new Glossary();
        $glossary->setTranslator($this->getUser());
        $glossary->setSource($this->container->get('app.source_repository')->find(1));

        $term = new Term();
        $term->setTranslator($this->getUser());
        $glossary->addTerm($term);

        // See https://symfony.com/doc/current/book/forms.html#submitting-forms-with-multiple-buttons
        $form = $this->createForm(GlossaryType::class, $glossary,[
			'display_term_locale' => false,
        	'locale_choices' => array_flip($locales),
		]);
        $form->handleRequest($request);

        // the isSubmitted() method is completely optional because the other
        // isValid() method already checks whether the form is submitted.
        // However, we explicitly add it to improve code readability.
        // See https://symfony.com/doc/current/best_practices/forms.html#handling-form-submits
        if ($form->isSubmitted() && $form->isValid()) {
        	foreach ($glossary->getTerms() as $term) {
        		$term->setLocale($glossary->getLocale());
        	}
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($glossary);
            $entityManager->flush();

            // Flash messages are used to notify the user about the result of the
            // actions. They are deleted automatically from the session as soon
            // as they are accessed.
            // See https://symfony.com/doc/current/book/controller.html#flash-messages
            $this->addFlash('success', 'post.created_successfully');
            return $this->redirectToRoute('glossary_index');
        }

        return $this->render('admin/glossary/new.html.twig', [
            'glossary' => $glossary,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_term_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Term $term, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(TermType::class, $term);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'Term updated successfully.');
            return $this->redirectToRoute('admin_term_edit', ['id' => $term->getId()]);
        }

        return $this->render('admin/term/edit.html.twig', [
        	'term' => $term,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Post entity.
     *
     * @Route("/{id}/delete", name="admin_term_delete")
     * @Method("POST")
     * @Security("user.getId() == term.getTranslator().getId() or has_role('ROLE_ADMIN')")
     *
     * The Security annotation value is an expression (if it evaluates to false,
     * the authorization mechanism will prevent the user accessing this resource).
     */
    public function deleteAction(Request $request, Term $term)
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('glossary_index');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($term);
        $entityManager->flush();

        $this->addFlash('success', 'Term deleted successfully.');
        return $this->redirectToRoute('glossary_index');
    }
}
