<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Term;
use AppBundle\Form\TermType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Glossary;

/**
 * Controller used to manage term contents in the backend.
 *
 * See http://knpbundles.com/keyword/admin
 *
 * @Route("/admin/term")
 * @Security("has_role('ROLE_ADMIN')")
 */
class TermController extends Controller
{
    /**
     * Creates a new Term entity.
     *
     * @Route("/new/{id}", name="admin_term_new", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     *
     * NOTE: the Method annotation is optional, but it's a recommended practice
     * to constraint the HTTP methods each controller responds to (by default
     * it responds to all methods).
     */
    public function newAction(Request $request, Glossary $glossary)
    {
        $term = new Term();
        $term->setTranslator($this->getUser());
        $term->setGlossary($glossary);
        if ($defaultTerm = $request->get('term')) {
        	$term->setTerm($defaultTerm);
        }

        // See https://symfony.com/doc/current/book/forms.html#submitting-forms-with-multiple-buttons
        $form = $this->createForm(TermType::class, $term);
        $form->handleRequest($request);

        // the isSubmitted() method is completely optional because the other
        // isValid() method already checks whether the form is submitted.
        // However, we explicitly add it to improve code readability.
        // See https://symfony.com/doc/current/best_practices/forms.html#handling-form-submits
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($term);
            $entityManager->flush();

            // Flash messages are used to notify the user about the result of the
            // actions. They are deleted automatically from the session as soon
            // as they are accessed.
            // See https://symfony.com/doc/current/book/controller.html#flash-messages
            $this->addFlash('success', 'post.created_successfully');

            return $this->redirectToRoute('glossary_index');
        }

        return $this->render('admin/term/new.html.twig', [
            'term' => $term,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_term_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Term $term, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(TermType::class, $term, ['disable_locale' => true]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'Term updated successfully.');
            return $this->redirectToRoute('admin_term_edit', ['id' => $term->getId()]);
        }

        return $this->render('admin/term/edit.html.twig', [
        	'term' => $term,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Post entity.
     *
     * @Route("/{id}/delete", name="admin_term_delete")
     * @Method("POST")
     * @Security("user.getId() == term.getTranslator().getId() or has_role('ROLE_ADMIN')")
     *
     * The Security annotation value is an expression (if it evaluates to false,
     * the authorization mechanism will prevent the user accessing this resource).
     */
    public function deleteAction(Request $request, Term $term)
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('glossary_index');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($term);
        $entityManager->flush();

        $this->addFlash('success', 'Term deleted successfully.');
        return $this->redirectToRoute('glossary_index');
    }
}
