<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Glossary;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * @Route("/glossary")
 */
class GlossaryController extends Controller
{
	public function filterGlossaryAction()
	{
		$localeRepo = $this->container->get('app.locale_repository');
		$localeChoices = ['All Locales' => ' '] + array_flip($localeRepo->findAll());

		$termRepo = $this->container->get('app.term_repository');
		$terms = ['All Terms' => ' '] + $termRepo->getFilterChoices();

		$builder = $this->createFormBuilder()
			->add('locale', ChoiceType::class, ['choices' => $localeChoices])
			->add('term', ChoiceType::class, ['choices' => $terms])
		;

		return $this->render(
				'glossary/_filter_form.html.twig',
				array('form' => $builder->getForm()->createView())
		);
	}

    /**
     * @Route("/", defaults={"page": "1", "_format"="html"}, name="glossary_index")
     * @Route("/page/{page}", defaults={"_format"="html"}, requirements={"page": "[1-9]\d*"}, name="glossary_index_paginated")
     */
	public function indexAction(Request $request, $page)
    {
    	$term = $request->get('term');
    	$locale = $request->get('locale');
    	if ($request->isMethod(Request::METHOD_POST)) {
    		if ($form = $request->get('form')) {
    			if (isset($form['locale']) && !empty(trim($form['locale']))) {
    				$locale = $form['locale'];
    			}
    			if (isset($form['term']) && !empty(trim($form['term']))) {
    				$term = $form['term'];
    			}
    		}
    	}
    	$glossaries = $this->container->get('app.glossary_repository')->findLatest($page, $term, $locale);
        return $this->render('glossary/index.html.twig', array(
        		'glossaries' => $glossaries,
        ));
    }
}
