Proz Demo Application
========================

Installation
------------

Be sure you have docker up and running on your local!

From the source root call the local and then go get a cup of tea for the installs =]

```
docker-compose up --build -d
```

Wait for the build to occur and the docker containers to be running. Now check with

```
docker ps
```

You should get an output similar to 

```

04:56:56 (master) ~/workspace/prozglossary$ docker ps
CONTAINER ID        IMAGE                       COMMAND                  CREATED             STATUS              PORTS                         NAMES
2547e9cf48f2        phpdockerio/nginx:latest    "/usr/sbin/nginx"        2 minutes ago       Up 7 seconds        0.0.0.0:80->80/tcp, 443/tcp   proz-webserver
8b77ac5c34b0        prozglossary_proz-php-fpm   "/bin/sh -c '/usr/..."   2 minutes ago       Up 7 seconds        9000/tcp                      proz-php-fpm
7aac04df9a60        mysql:5.7                   "docker-entrypoint..."   2 minutes ago       Up 8 seconds        0.0.0.0:3306->3306/tcp        proz-mysql


```

Now run an interactive shell on the `proz-webserver` container 

```
docker exec -it proz-php-fpm sh
```

Then cd into the working dir and start buildig and loading the fixtures.

```
$ cd /var/www/proz/

  - Note composer will likely take a while...
  
$ composer install --prefer-source --no-interaction  

$ php bin/console doctrine:database:create

$ php bin/console doctrine:schema:create

$ php bin/console doctrine:fixtures:load

```

Usage
-----

Note that nginx is listening on port 80 so open a web browser and goto [http://0.0.0.0:80](http://0.0.0.0:80) to view the demo.

Stop with `docker-compose stop`.

