<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_e15a66e133b9440d8c90e588488b68699c1e9ab179ecd26e7dfb8116728e8a1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41f90e1cdb7ddacde87aecc39487805f456b88b1bc659e4850e1bdb6ce783698 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_41f90e1cdb7ddacde87aecc39487805f456b88b1bc659e4850e1bdb6ce783698->enter($__internal_41f90e1cdb7ddacde87aecc39487805f456b88b1bc659e4850e1bdb6ce783698_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_1592299f08bbd7176416944eaf37d1ecb2aae25b026cd748e9efa915a0b87833 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1592299f08bbd7176416944eaf37d1ecb2aae25b026cd748e9efa915a0b87833->enter($__internal_1592299f08bbd7176416944eaf37d1ecb2aae25b026cd748e9efa915a0b87833_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_41f90e1cdb7ddacde87aecc39487805f456b88b1bc659e4850e1bdb6ce783698->leave($__internal_41f90e1cdb7ddacde87aecc39487805f456b88b1bc659e4850e1bdb6ce783698_prof);

        
        $__internal_1592299f08bbd7176416944eaf37d1ecb2aae25b026cd748e9efa915a0b87833->leave($__internal_1592299f08bbd7176416944eaf37d1ecb2aae25b026cd748e9efa915a0b87833_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_a9e6fe650e72397f7974d1c985c7193f686e24a1b56f8fffecb658ffacdbc41f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9e6fe650e72397f7974d1c985c7193f686e24a1b56f8fffecb658ffacdbc41f->enter($__internal_a9e6fe650e72397f7974d1c985c7193f686e24a1b56f8fffecb658ffacdbc41f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_6c6929e0639b6f4cff4f1baa472d88149465afd6aa5c8f69510844931f698895 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c6929e0639b6f4cff4f1baa472d88149465afd6aa5c8f69510844931f698895->enter($__internal_6c6929e0639b6f4cff4f1baa472d88149465afd6aa5c8f69510844931f698895_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_6c6929e0639b6f4cff4f1baa472d88149465afd6aa5c8f69510844931f698895->leave($__internal_6c6929e0639b6f4cff4f1baa472d88149465afd6aa5c8f69510844931f698895_prof);

        
        $__internal_a9e6fe650e72397f7974d1c985c7193f686e24a1b56f8fffecb658ffacdbc41f->leave($__internal_a9e6fe650e72397f7974d1c985c7193f686e24a1b56f8fffecb658ffacdbc41f_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/var/www/proz/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
