<?php

/* base.html.twig */
class __TwigTemplate_36d408e040da5b3c428bf01363eae6c5e0a286597485fe324e7f56343b0d6beb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'header_navigation_links' => array($this, 'block_header_navigation_links'),
            'body' => array($this, 'block_body'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bb06c4a1787770bb46b02b26fa4330deaf2e7f927b42a120396d172de381d62b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb06c4a1787770bb46b02b26fa4330deaf2e7f927b42a120396d172de381d62b->enter($__internal_bb06c4a1787770bb46b02b26fa4330deaf2e7f927b42a120396d172de381d62b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_e41b8ec6d6ef2e1cb9a02b1cb680aa7e220755e51e0801b658685ff61e1fcf64 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e41b8ec6d6ef2e1cb9a02b1cb680aa7e220755e51e0801b658685ff61e1fcf64->enter($__internal_e41b8ec6d6ef2e1cb9a02b1cb680aa7e220755e51e0801b658685ff61e1fcf64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 6
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "locale", array()), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 21
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>

    <body id=\"";
        // line 24
        $this->displayBlock('body_id', $context, $blocks);
        echo "\">

        ";
        // line 26
        $this->displayBlock('header', $context, $blocks);
        // line 80
        echo "
        <div class=\"container body-container\">
            ";
        // line 82
        $this->displayBlock('body', $context, $blocks);
        // line 97
        echo "        </div>

        ";
        // line 99
        $this->displayBlock('footer', $context, $blocks);
        // line 111
        echo "
        ";
        // line 112
        $this->displayBlock('javascripts', $context, $blocks);
        // line 121
        echo "
        ";
        // line 125
        echo "        <!-- Page rendered on ";
        echo twig_escape_filter($this->env, twig_localized_date_filter($this->env, "now", "long", "long", null, "UTC"), "html", null, true);
        echo " -->
    </body>
</html>
";
        
        $__internal_bb06c4a1787770bb46b02b26fa4330deaf2e7f927b42a120396d172de381d62b->leave($__internal_bb06c4a1787770bb46b02b26fa4330deaf2e7f927b42a120396d172de381d62b_prof);

        
        $__internal_e41b8ec6d6ef2e1cb9a02b1cb680aa7e220755e51e0801b658685ff61e1fcf64->leave($__internal_e41b8ec6d6ef2e1cb9a02b1cb680aa7e220755e51e0801b658685ff61e1fcf64_prof);

    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        $__internal_a6e1ab57ef01a4f7736ea4eeee0c7881437a909dbc366ba5bfdb4d661cc504b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6e1ab57ef01a4f7736ea4eeee0c7881437a909dbc366ba5bfdb4d661cc504b8->enter($__internal_a6e1ab57ef01a4f7736ea4eeee0c7881437a909dbc366ba5bfdb4d661cc504b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_c56b2af94ad398283f3b0d534e61f0076bbd9dbe778441d0199e382171b88f9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c56b2af94ad398283f3b0d534e61f0076bbd9dbe778441d0199e382171b88f9f->enter($__internal_c56b2af94ad398283f3b0d534e61f0076bbd9dbe778441d0199e382171b88f9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Proz Glossary";
        
        $__internal_c56b2af94ad398283f3b0d534e61f0076bbd9dbe778441d0199e382171b88f9f->leave($__internal_c56b2af94ad398283f3b0d534e61f0076bbd9dbe778441d0199e382171b88f9f_prof);

        
        $__internal_a6e1ab57ef01a4f7736ea4eeee0c7881437a909dbc366ba5bfdb4d661cc504b8->leave($__internal_a6e1ab57ef01a4f7736ea4eeee0c7881437a909dbc366ba5bfdb4d661cc504b8_prof);

    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_956783601cd2c8f2e73bd1ce3543ad6682394cba51726b82fb9e517e06d8b9f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_956783601cd2c8f2e73bd1ce3543ad6682394cba51726b82fb9e517e06d8b9f6->enter($__internal_956783601cd2c8f2e73bd1ce3543ad6682394cba51726b82fb9e517e06d8b9f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_4e0ab5def747f6d157535b7037ace8ec9b0338a03c148639aa40239c092280e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e0ab5def747f6d157535b7037ace8ec9b0338a03c148639aa40239c092280e2->enter($__internal_4e0ab5def747f6d157535b7037ace8ec9b0338a03c148639aa40239c092280e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 13
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-flatly-3.3.7.min.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-awesome-4.6.3.min.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-lato.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/highlight-solarized-light.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-tagsinput.css"), "html", null, true);
        echo "\">
            <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/main.css"), "html", null, true);
        echo "\">
        ";
        
        $__internal_4e0ab5def747f6d157535b7037ace8ec9b0338a03c148639aa40239c092280e2->leave($__internal_4e0ab5def747f6d157535b7037ace8ec9b0338a03c148639aa40239c092280e2_prof);

        
        $__internal_956783601cd2c8f2e73bd1ce3543ad6682394cba51726b82fb9e517e06d8b9f6->leave($__internal_956783601cd2c8f2e73bd1ce3543ad6682394cba51726b82fb9e517e06d8b9f6_prof);

    }

    // line 24
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_aeeee01b08f206bc2b741c60f656f16e0d1644275ce125b64e4cae761e14ca2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aeeee01b08f206bc2b741c60f656f16e0d1644275ce125b64e4cae761e14ca2c->enter($__internal_aeeee01b08f206bc2b741c60f656f16e0d1644275ce125b64e4cae761e14ca2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_b13848ffb9a736f811adc3442cd352ce13d62c207aef0e900df470a89011c1ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b13848ffb9a736f811adc3442cd352ce13d62c207aef0e900df470a89011c1ba->enter($__internal_b13848ffb9a736f811adc3442cd352ce13d62c207aef0e900df470a89011c1ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_b13848ffb9a736f811adc3442cd352ce13d62c207aef0e900df470a89011c1ba->leave($__internal_b13848ffb9a736f811adc3442cd352ce13d62c207aef0e900df470a89011c1ba_prof);

        
        $__internal_aeeee01b08f206bc2b741c60f656f16e0d1644275ce125b64e4cae761e14ca2c->leave($__internal_aeeee01b08f206bc2b741c60f656f16e0d1644275ce125b64e4cae761e14ca2c_prof);

    }

    // line 26
    public function block_header($context, array $blocks = array())
    {
        $__internal_4a32cdf94b95a627063b9d9140b40d8ef84ace64ddf9a05e823d0f1fd72edbab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a32cdf94b95a627063b9d9140b40d8ef84ace64ddf9a05e823d0f1fd72edbab->enter($__internal_4a32cdf94b95a627063b9d9140b40d8ef84ace64ddf9a05e823d0f1fd72edbab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_769754b391704fcd6bd30697df84b82a726e69b7a5e0a3c0ee868180a940974b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_769754b391704fcd6bd30697df84b82a726e69b7a5e0a3c0ee868180a940974b->enter($__internal_769754b391704fcd6bd30697df84b82a726e69b7a5e0a3c0ee868180a940974b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 27
        echo "            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                Proz Glossary
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("menu.toggle_nav"), "html", null, true);
        echo "</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">

                                ";
        // line 46
        $this->displayBlock('header_navigation_links', $context, $blocks);
        // line 53
        echo "
                                ";
        // line 54
        if ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
            // line 55
            echo "                                    <li>
                                        <a href=\"";
            // line 56
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_logout");
            echo "\">
                                            <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i> ";
            // line 57
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("menu.logout"), "html", null, true);
            echo "
                                        </a>
                                    </li>
                                ";
        }
        // line 61
        echo "
                                <li class=\"dropdown\">
                                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\" id=\"locales\">
                                        <i class=\"fa fa-globe\" aria-hidden=\"true\"></i>
                                        <span class=\"caret\"></span>
                                        <span class=\"sr-only\">";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("menu.choose_language"), "html", null, true);
        echo "</span>
                                    </a>
                                    <ul class=\"dropdown-menu locales\" role=\"menu\" aria-labelledby=\"locales\">
                                        ";
        // line 69
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('AppBundle\Twig\AppExtension')->getLocales());
        foreach ($context['_seq'] as $context["_key"] => $context["locale"]) {
            // line 70
            echo "                                            <li ";
            if (($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "locale", array()) == $this->getAttribute($context["locale"], "code", array()))) {
                echo "aria-checked=\"true\" class=\"active\"";
            } else {
                echo "aria-checked=\"false\"";
            }
            echo " role=\"menuitem\"><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route", 1 => "blog_index"), "method"), twig_array_merge($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route_params", 1 => array()), "method"), array("_locale" => $this->getAttribute($context["locale"], "code", array())))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($context["locale"], "name", array())), "html", null, true);
            echo "</a></li>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['locale'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        ";
        
        $__internal_769754b391704fcd6bd30697df84b82a726e69b7a5e0a3c0ee868180a940974b->leave($__internal_769754b391704fcd6bd30697df84b82a726e69b7a5e0a3c0ee868180a940974b_prof);

        
        $__internal_4a32cdf94b95a627063b9d9140b40d8ef84ace64ddf9a05e823d0f1fd72edbab->leave($__internal_4a32cdf94b95a627063b9d9140b40d8ef84ace64ddf9a05e823d0f1fd72edbab_prof);

    }

    // line 46
    public function block_header_navigation_links($context, array $blocks = array())
    {
        $__internal_eaf0341ba9eb93cc173669dd836e33d62c8d430dcca6cb7f2552a53ba37ad437 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eaf0341ba9eb93cc173669dd836e33d62c8d430dcca6cb7f2552a53ba37ad437->enter($__internal_eaf0341ba9eb93cc173669dd836e33d62c8d430dcca6cb7f2552a53ba37ad437_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        $__internal_7528d4898c6912a3e9e01a4e5c90191664e342e5a1bae0103da33957ba558543 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7528d4898c6912a3e9e01a4e5c90191664e342e5a1bae0103da33957ba558543->enter($__internal_7528d4898c6912a3e9e01a4e5c90191664e342e5a1bae0103da33957ba558543_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        // line 47
        echo "                                    <li>
                                        <a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i> ";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("menu.homepage"), "html", null, true);
        echo "
                                        </a>
                                    </li>
                                ";
        
        $__internal_7528d4898c6912a3e9e01a4e5c90191664e342e5a1bae0103da33957ba558543->leave($__internal_7528d4898c6912a3e9e01a4e5c90191664e342e5a1bae0103da33957ba558543_prof);

        
        $__internal_eaf0341ba9eb93cc173669dd836e33d62c8d430dcca6cb7f2552a53ba37ad437->leave($__internal_eaf0341ba9eb93cc173669dd836e33d62c8d430dcca6cb7f2552a53ba37ad437_prof);

    }

    // line 82
    public function block_body($context, array $blocks = array())
    {
        $__internal_0e5003311384931ce8ca846aceb476a64327fc51194300b0dbd3174c5b083dc7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0e5003311384931ce8ca846aceb476a64327fc51194300b0dbd3174c5b083dc7->enter($__internal_0e5003311384931ce8ca846aceb476a64327fc51194300b0dbd3174c5b083dc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_fbf9aad688a726c442a6e27e30cc9fd304e40b57e3171f5439538d4e0f1c791a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fbf9aad688a726c442a6e27e30cc9fd304e40b57e3171f5439538d4e0f1c791a->enter($__internal_fbf9aad688a726c442a6e27e30cc9fd304e40b57e3171f5439538d4e0f1c791a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 83
        echo "                <div class=\"row\">
                    <div id=\"main\" class=\"col-sm-9\">
                        ";
        // line 85
        echo twig_include($this->env, $context, "default/_flash_messages.html.twig");
        echo "
                        
                        ";
        // line 87
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(Symfony\Bridge\Twig\Extension\HttpKernelExtension::controller("AppBundle:Source:defaultSource"));
        echo "

                        ";
        // line 89
        $this->displayBlock('main', $context, $blocks);
        // line 90
        echo "                    </div>

                    <div id=\"sidebar\" class=\"col-sm-3\">
                        ";
        // line 93
        $this->displayBlock('sidebar', $context, $blocks);
        // line 94
        echo "                    </div>
                </div>
            ";
        
        $__internal_fbf9aad688a726c442a6e27e30cc9fd304e40b57e3171f5439538d4e0f1c791a->leave($__internal_fbf9aad688a726c442a6e27e30cc9fd304e40b57e3171f5439538d4e0f1c791a_prof);

        
        $__internal_0e5003311384931ce8ca846aceb476a64327fc51194300b0dbd3174c5b083dc7->leave($__internal_0e5003311384931ce8ca846aceb476a64327fc51194300b0dbd3174c5b083dc7_prof);

    }

    // line 89
    public function block_main($context, array $blocks = array())
    {
        $__internal_05bc2b004ca697e5256701697fe78d74729bd93c226ca90ae9e556c35bac5166 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_05bc2b004ca697e5256701697fe78d74729bd93c226ca90ae9e556c35bac5166->enter($__internal_05bc2b004ca697e5256701697fe78d74729bd93c226ca90ae9e556c35bac5166_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_5b353225da59881218cc3b610a34386063deb2e43037f9e44823d0d432174d6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b353225da59881218cc3b610a34386063deb2e43037f9e44823d0d432174d6c->enter($__internal_5b353225da59881218cc3b610a34386063deb2e43037f9e44823d0d432174d6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        
        $__internal_5b353225da59881218cc3b610a34386063deb2e43037f9e44823d0d432174d6c->leave($__internal_5b353225da59881218cc3b610a34386063deb2e43037f9e44823d0d432174d6c_prof);

        
        $__internal_05bc2b004ca697e5256701697fe78d74729bd93c226ca90ae9e556c35bac5166->leave($__internal_05bc2b004ca697e5256701697fe78d74729bd93c226ca90ae9e556c35bac5166_prof);

    }

    // line 93
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_44a2ee6221f06a70187310b0a87b70b466b6b51d2676928f241ad806656873a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44a2ee6221f06a70187310b0a87b70b466b6b51d2676928f241ad806656873a1->enter($__internal_44a2ee6221f06a70187310b0a87b70b466b6b51d2676928f241ad806656873a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_edd3dc839051f01655e74005ca69d57bbb6b7527d3c77539bd1626a837b712df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_edd3dc839051f01655e74005ca69d57bbb6b7527d3c77539bd1626a837b712df->enter($__internal_edd3dc839051f01655e74005ca69d57bbb6b7527d3c77539bd1626a837b712df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        
        $__internal_edd3dc839051f01655e74005ca69d57bbb6b7527d3c77539bd1626a837b712df->leave($__internal_edd3dc839051f01655e74005ca69d57bbb6b7527d3c77539bd1626a837b712df_prof);

        
        $__internal_44a2ee6221f06a70187310b0a87b70b466b6b51d2676928f241ad806656873a1->leave($__internal_44a2ee6221f06a70187310b0a87b70b466b6b51d2676928f241ad806656873a1_prof);

    }

    // line 99
    public function block_footer($context, array $blocks = array())
    {
        $__internal_607dee4381280b9b14aa0a16525ad5fd09fa9ca7f0c31f58f47a7afa4ee47399 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_607dee4381280b9b14aa0a16525ad5fd09fa9ca7f0c31f58f47a7afa4ee47399->enter($__internal_607dee4381280b9b14aa0a16525ad5fd09fa9ca7f0c31f58f47a7afa4ee47399_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_3ee8d71aaf8f5c5569b71ced7d419611180dd904c9a6a1fb0eaa21a15a9ebcbe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ee8d71aaf8f5c5569b71ced7d419611180dd904c9a6a1fb0eaa21a15a9ebcbe->enter($__internal_3ee8d71aaf8f5c5569b71ced7d419611180dd904c9a6a1fb0eaa21a15a9ebcbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 100
        echo "            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-6\">
                            <p>&copy; ";
        // line 104
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " - A Proz Project built with Symfony</p>
                            <p>";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("mit_license"), "html", null, true);
        echo "</p>
                        </div>
                    </div>
                </div>
            </footer>
        ";
        
        $__internal_3ee8d71aaf8f5c5569b71ced7d419611180dd904c9a6a1fb0eaa21a15a9ebcbe->leave($__internal_3ee8d71aaf8f5c5569b71ced7d419611180dd904c9a6a1fb0eaa21a15a9ebcbe_prof);

        
        $__internal_607dee4381280b9b14aa0a16525ad5fd09fa9ca7f0c31f58f47a7afa4ee47399->leave($__internal_607dee4381280b9b14aa0a16525ad5fd09fa9ca7f0c31f58f47a7afa4ee47399_prof);

    }

    // line 112
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_87932ebff82a076527e0d7e184b1012a5eff4df54dfff12c43e79112096bb158 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87932ebff82a076527e0d7e184b1012a5eff4df54dfff12c43e79112096bb158->enter($__internal_87932ebff82a076527e0d7e184b1012a5eff4df54dfff12c43e79112096bb158_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_e5605ef4b0a3aa940f2c3f2155c32633ec6df4e9978cd29b884722e514525186 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5605ef4b0a3aa940f2c3f2155c32633ec6df4e9978cd29b884722e514525186->enter($__internal_e5605ef4b0a3aa940f2c3f2155c32633ec6df4e9978cd29b884722e514525186_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 113
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.4.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/moment.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-3.3.7.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/highlight.pack.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-tagsinput.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_e5605ef4b0a3aa940f2c3f2155c32633ec6df4e9978cd29b884722e514525186->leave($__internal_e5605ef4b0a3aa940f2c3f2155c32633ec6df4e9978cd29b884722e514525186_prof);

        
        $__internal_87932ebff82a076527e0d7e184b1012a5eff4df54dfff12c43e79112096bb158->leave($__internal_87932ebff82a076527e0d7e184b1012a5eff4df54dfff12c43e79112096bb158_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  466 => 119,  462 => 118,  458 => 117,  454 => 116,  450 => 115,  446 => 114,  441 => 113,  432 => 112,  416 => 105,  412 => 104,  406 => 100,  397 => 99,  380 => 93,  363 => 89,  351 => 94,  349 => 93,  344 => 90,  342 => 89,  337 => 87,  332 => 85,  328 => 83,  319 => 82,  305 => 49,  301 => 48,  298 => 47,  289 => 46,  272 => 72,  255 => 70,  251 => 69,  245 => 66,  238 => 61,  231 => 57,  227 => 56,  224 => 55,  222 => 54,  219 => 53,  217 => 46,  205 => 37,  196 => 31,  190 => 27,  181 => 26,  164 => 24,  152 => 19,  148 => 18,  144 => 17,  140 => 16,  136 => 15,  132 => 14,  127 => 13,  118 => 12,  100 => 11,  85 => 125,  82 => 121,  80 => 112,  77 => 111,  75 => 99,  71 => 97,  69 => 82,  65 => 80,  63 => 26,  58 => 24,  51 => 21,  49 => 12,  45 => 11,  38 => 7,  35 => 6,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
   This is the base template used as the application layout which contains the
   common elements and decorates all the other templates.
   See https://symfony.com/doc/current/book/templating.html#template-inheritance-and-layouts
#}
<!DOCTYPE html>
<html lang=\"{{ app.request.locale }}\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>{% block title %}Proz Glossary{% endblock %}</title>
        {% block stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-flatly-3.3.7.min.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/font-awesome-4.6.3.min.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/font-lato.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-datetimepicker.min.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/highlight-solarized-light.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-tagsinput.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/main.css') }}\">
        {% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>

    <body id=\"{% block body_id %}{% endblock %}\">

        {% block header %}
            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"{{ path('homepage') }}\">
                                Proz Glossary
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">{{ 'menu.toggle_nav'|trans }}</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">

                                {% block header_navigation_links %}
                                    <li>
                                        <a href=\"{{ path('homepage') }}\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i> {{ 'menu.homepage'|trans }}
                                        </a>
                                    </li>
                                {% endblock %}

                                {% if app.user %}
                                    <li>
                                        <a href=\"{{ path('security_logout') }}\">
                                            <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i> {{ 'menu.logout'|trans }}
                                        </a>
                                    </li>
                                {% endif %}

                                <li class=\"dropdown\">
                                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\" id=\"locales\">
                                        <i class=\"fa fa-globe\" aria-hidden=\"true\"></i>
                                        <span class=\"caret\"></span>
                                        <span class=\"sr-only\">{{ 'menu.choose_language'|trans }}</span>
                                    </a>
                                    <ul class=\"dropdown-menu locales\" role=\"menu\" aria-labelledby=\"locales\">
                                        {% for locale in locales() %}
                                            <li {% if app.request.locale == locale.code %}aria-checked=\"true\" class=\"active\"{% else %}aria-checked=\"false\"{% endif %} role=\"menuitem\"><a href=\"{{ path(app.request.get('_route', 'blog_index'), app.request.get('_route_params', [])|merge({_locale: locale.code})) }}\">{{ locale.name|capitalize }}</a></li>
                                        {% endfor %}
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        {% endblock %}

        <div class=\"container body-container\">
            {% block body %}
                <div class=\"row\">
                    <div id=\"main\" class=\"col-sm-9\">
                        {{ include('default/_flash_messages.html.twig') }}
                        
                        {{ render(controller('AppBundle:Source:defaultSource')) }}

                        {% block main %}{% endblock %}
                    </div>

                    <div id=\"sidebar\" class=\"col-sm-3\">
                        {% block sidebar %}{% endblock %}
                    </div>
                </div>
            {% endblock %}
        </div>

        {% block footer %}
            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-6\">
                            <p>&copy; {{ 'now'|date('Y') }} - A Proz Project built with Symfony</p>
                            <p>{{ 'mit_license'|trans }}</p>
                        </div>
                    </div>
                </div>
            </footer>
        {% endblock %}

        {% block javascripts %}
            <script src=\"{{ asset('js/jquery-2.2.4.min.js') }}\"></script>
            <script src=\"{{ asset('js/moment.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-3.3.7.min.js') }}\"></script>
            <script src=\"{{ asset('js/highlight.pack.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-datetimepicker.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-tagsinput.min.js') }}\"></script>
            <script src=\"{{ asset('js/main.js') }}\"></script>
        {% endblock %}

        {# it's not mandatory to set the timezone in localizeddate(). This is done to
           avoid errors when the 'intl' PHP extension is not available and the application
           is forced to use the limited \"intl polyfill\", which only supports UTC and GMT #}
        <!-- Page rendered on {{ 'now'|localizeddate('long', 'long', null, 'UTC') }} -->
    </body>
</html>
", "base.html.twig", "/var/www/proz/app/Resources/views/base.html.twig");
    }
}
