<?php

/* glossary/index.html.twig */
class __TwigTemplate_aa360aea23d9f8d36099ac172c1dc0914d082e7410421db0d553a6fc4387a3c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "glossary/index.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b2d642dcba27141964eea19357b1f823b0c11b246ba588398d2efeecb49ca843 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b2d642dcba27141964eea19357b1f823b0c11b246ba588398d2efeecb49ca843->enter($__internal_b2d642dcba27141964eea19357b1f823b0c11b246ba588398d2efeecb49ca843_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "glossary/index.html.twig"));

        $__internal_0532084094793b30f524ad0be0c9083afb63bdd6d8c9482c0d7206eac4c096fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0532084094793b30f524ad0be0c9083afb63bdd6d8c9482c0d7206eac4c096fb->enter($__internal_0532084094793b30f524ad0be0c9083afb63bdd6d8c9482c0d7206eac4c096fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "glossary/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b2d642dcba27141964eea19357b1f823b0c11b246ba588398d2efeecb49ca843->leave($__internal_b2d642dcba27141964eea19357b1f823b0c11b246ba588398d2efeecb49ca843_prof);

        
        $__internal_0532084094793b30f524ad0be0c9083afb63bdd6d8c9482c0d7206eac4c096fb->leave($__internal_0532084094793b30f524ad0be0c9083afb63bdd6d8c9482c0d7206eac4c096fb_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_3bbcffe9ea654f76512c4ba622f07f62976b2d5906d610b230a116a31f2d4920 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3bbcffe9ea654f76512c4ba622f07f62976b2d5906d610b230a116a31f2d4920->enter($__internal_3bbcffe9ea654f76512c4ba622f07f62976b2d5906d610b230a116a31f2d4920_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_4ef6eb53719396de8bec08b16cd6da049225d0d543e0706ba56b1a83ebff4937 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ef6eb53719396de8bec08b16cd6da049225d0d543e0706ba56b1a83ebff4937->enter($__internal_4ef6eb53719396de8bec08b16cd6da049225d0d543e0706ba56b1a83ebff4937_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "glossary_index";
        
        $__internal_4ef6eb53719396de8bec08b16cd6da049225d0d543e0706ba56b1a83ebff4937->leave($__internal_4ef6eb53719396de8bec08b16cd6da049225d0d543e0706ba56b1a83ebff4937_prof);

        
        $__internal_3bbcffe9ea654f76512c4ba622f07f62976b2d5906d610b230a116a31f2d4920->leave($__internal_3bbcffe9ea654f76512c4ba622f07f62976b2d5906d610b230a116a31f2d4920_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_37a474622a6dd8d45398aaec5d0635596ef49f808dada1ebf8456d0c9ef69b4a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_37a474622a6dd8d45398aaec5d0635596ef49f808dada1ebf8456d0c9ef69b4a->enter($__internal_37a474622a6dd8d45398aaec5d0635596ef49f808dada1ebf8456d0c9ef69b4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_7288b0a65734613f75310265905f32de31c5d0dcb78f95dd4bd6976f1e0e4e22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7288b0a65734613f75310265905f32de31c5d0dcb78f95dd4bd6976f1e0e4e22->enter($__internal_7288b0a65734613f75310265905f32de31c5d0dcb78f95dd4bd6976f1e0e4e22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    <h2>Glossaries</h2>
    ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(Symfony\Bridge\Twig\Extension\HttpKernelExtension::controller("AppBundle:Glossary:filterGlossary"));
        echo "
    
    ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["glossaries"] ?? $this->getContext($context, "glossaries")));
        foreach ($context['_seq'] as $context["_key"] => $context["glossary"]) {
            // line 10
            echo "    ";
            $context["terms"] = $this->getAttribute($context["glossary"], "terms", array());
            // line 11
            echo "    ";
            $context["locales"] = $this->env->getExtension('AppBundle\Twig\AppExtension')->getLocales();
            // line 12
            echo "    <table class=\"table table-striped table-middle-aligned\">
        <thead>
            <tr>
                <th colspan=\"4\">A glossary based on ";
            // line 15
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["locales"] ?? $this->getContext($context, "locales")), $this->getAttribute($context["glossary"], "locale", array()), array(), "array"), "name", array())), "html", null, true);
            echo " by ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["glossary"], "translator", array()), "fullName", array()), "html", null, true);
            echo "</div>
            </tr>
            <tr>
                <th scope=\"col\" class=\"col-sm-2\">Term</th>
                <th scope=\"col\" class=\"col-sm-2\">Locale</th>
                <th scope=\"col\" class=\"col-sm-8\">Definition</th>
                <th scope=\"col\" class=\"col-sm-2 text-center\"><i class=\"fa fa-cogs\" aria-hidden=\"true\"></i> ";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.actions"), "html", null, true);
            echo "</th>
            </tr>
        </thead>
        <tbody>
        ";
            // line 25
            list($context["cur"], $context["prev"]) =             array("", "");
            // line 26
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["terms"] ?? $this->getContext($context, "terms")));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["term"]) {
                // line 27
                echo "            ";
                $context["cur"] = $this->getAttribute($context["term"], "term", array());
                // line 28
                echo "            <tr>
                <td>
                    ";
                // line 30
                if ((($context["cur"] ?? $this->getContext($context, "cur")) != ($context["prev"] ?? $this->getContext($context, "prev")))) {
                    // line 31
                    echo "                        ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["term"], "term", array()), "html", null, true);
                    echo "
                    ";
                }
                // line 33
                echo "                    ";
                $context["prev"] = ($context["cur"] ?? $this->getContext($context, "cur"));
                // line 34
                echo "                </td>
                <td>";
                // line 35
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["locales"] ?? $this->getContext($context, "locales")), $this->getAttribute($context["term"], "locale", array()), array(), "array"), "name", array())), "html", null, true);
                echo "</td>
                <td>";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute($context["term"], "definition", array()), "html", null, true);
                echo "</td>
                <td class=\"text-right text-nowrap\">
                    <div class=\"item-actions\">
                        <a href=\"";
                // line 39
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_term_edit", array("id" => $this->getAttribute($context["term"], "id", array()))), "html", null, true);
                echo "\" class=\"btn btn-sm btn-primary\">
                            <i class=\"fa fa-edit\" aria-hidden=\"true\"></i>
                        </a>
                        <a href=\"";
                // line 42
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_term_new", array("id" => $this->getAttribute($context["glossary"], "id", array()), "term" => $this->getAttribute($context["term"], "term", array()))), "html", null, true);
                echo "\" class=\"btn btn-sm btn-primary\">
                            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i>
                        </a>
                    </div>
                </td>
            </tr>
        ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 49
                echo "            <tr>
                <td colspan=\"4\" align=\"center\">";
                // line 50
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("post.no_posts_found"), "html", null, true);
                echo "</td>
           </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['term'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "        
        </tbody>
    </table>
    <hr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['glossary'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "    <div>
        <a href=\"";
        // line 58
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_glossary_new");
        echo "\" class=\"btn btn-sm btn-primary\">
            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i> New Glossary
        </a>
    </div>
";
        
        $__internal_7288b0a65734613f75310265905f32de31c5d0dcb78f95dd4bd6976f1e0e4e22->leave($__internal_7288b0a65734613f75310265905f32de31c5d0dcb78f95dd4bd6976f1e0e4e22_prof);

        
        $__internal_37a474622a6dd8d45398aaec5d0635596ef49f808dada1ebf8456d0c9ef69b4a->leave($__internal_37a474622a6dd8d45398aaec5d0635596ef49f808dada1ebf8456d0c9ef69b4a_prof);

    }

    // line 64
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_4e9c6ff271d565e8f191c4a08815750150c8c5f866b22c314f81c675d2f8a781 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4e9c6ff271d565e8f191c4a08815750150c8c5f866b22c314f81c675d2f8a781->enter($__internal_4e9c6ff271d565e8f191c4a08815750150c8c5f866b22c314f81c675d2f8a781_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_ccaabd292770e24861c69e6a7660d61827fdcc82a7a60e7068669956605275a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ccaabd292770e24861c69e6a7660d61827fdcc82a7a60e7068669956605275a7->enter($__internal_ccaabd292770e24861c69e6a7660d61827fdcc82a7a60e7068669956605275a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 65
        echo "    ";
        $this->displayParentBlock("sidebar", $context, $blocks);
        echo "
    ";
        // line 66
        echo $this->env->getExtension('CodeExplorerBundle\Twig\SourceCodeExtension')->showSourceCode($this->env, $this);
        echo "
";
        
        $__internal_ccaabd292770e24861c69e6a7660d61827fdcc82a7a60e7068669956605275a7->leave($__internal_ccaabd292770e24861c69e6a7660d61827fdcc82a7a60e7068669956605275a7_prof);

        
        $__internal_4e9c6ff271d565e8f191c4a08815750150c8c5f866b22c314f81c675d2f8a781->leave($__internal_4e9c6ff271d565e8f191c4a08815750150c8c5f866b22c314f81c675d2f8a781_prof);

    }

    public function getTemplateName()
    {
        return "glossary/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 66,  216 => 65,  207 => 64,  192 => 58,  189 => 57,  179 => 52,  170 => 50,  167 => 49,  155 => 42,  149 => 39,  143 => 36,  139 => 35,  136 => 34,  133 => 33,  127 => 31,  125 => 30,  121 => 28,  118 => 27,  112 => 26,  110 => 25,  103 => 21,  92 => 15,  87 => 12,  84 => 11,  81 => 10,  77 => 9,  72 => 7,  69 => 6,  60 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'glossary_index' %}

{% block main %}
    <h2>Glossaries</h2>
    {{ render(controller('AppBundle:Glossary:filterGlossary')) }}
    
    {% for glossary in glossaries %}
    {% set terms = glossary.terms %}
    {% set locales = locales() %}
    <table class=\"table table-striped table-middle-aligned\">
        <thead>
            <tr>
                <th colspan=\"4\">A glossary based on {{ locales[glossary.locale].name|capitalize }} by {{ glossary.translator.fullName }}</div>
            </tr>
            <tr>
                <th scope=\"col\" class=\"col-sm-2\">Term</th>
                <th scope=\"col\" class=\"col-sm-2\">Locale</th>
                <th scope=\"col\" class=\"col-sm-8\">Definition</th>
                <th scope=\"col\" class=\"col-sm-2 text-center\"><i class=\"fa fa-cogs\" aria-hidden=\"true\"></i> {{ 'label.actions'|trans }}</th>
            </tr>
        </thead>
        <tbody>
        {% set cur, prev = \"\", \"\" %}
        {% for term in terms %}
            {% set cur = term.term %}
            <tr>
                <td>
                    {% if cur != prev %}
                        {{ term.term }}
                    {% endif %}
                    {% set prev = cur %}
                </td>
                <td>{{ locales[term.locale].name|capitalize }}</td>
                <td>{{ term.definition }}</td>
                <td class=\"text-right text-nowrap\">
                    <div class=\"item-actions\">
                        <a href=\"{{ path('admin_term_edit', {id: term.id}) }}\" class=\"btn btn-sm btn-primary\">
                            <i class=\"fa fa-edit\" aria-hidden=\"true\"></i>
                        </a>
                        <a href=\"{{ path('admin_term_new', {id: glossary.id, term: term.term }) }}\" class=\"btn btn-sm btn-primary\">
                            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i>
                        </a>
                    </div>
                </td>
            </tr>
        {% else %}
            <tr>
                <td colspan=\"4\" align=\"center\">{{ 'post.no_posts_found'|trans }}</td>
           </tr>
        {% endfor %}        
        </tbody>
    </table>
    <hr>
    {% endfor %}
    <div>
        <a href=\"{{ path('admin_glossary_new') }}\" class=\"btn btn-sm btn-primary\">
            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i> New Glossary
        </a>
    </div>
{% endblock %}

{% block sidebar %}
    {{ parent() }}
    {{ show_source_code(_self) }}
{% endblock %}", "glossary/index.html.twig", "/var/www/proz/app/Resources/views/glossary/index.html.twig");
    }
}
