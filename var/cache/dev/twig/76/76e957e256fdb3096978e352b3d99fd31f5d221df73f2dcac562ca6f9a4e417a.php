<?php

/* glossary/_filter_form.html.twig */
class __TwigTemplate_a9b3af90dc032dd747425268459de63ec68fd74e5b75d3810b1754952ae7e06e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_524397b57fbed584a9955871e01f703fb251217fbca6bd0b640612f8d4414c91 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_524397b57fbed584a9955871e01f703fb251217fbca6bd0b640612f8d4414c91->enter($__internal_524397b57fbed584a9955871e01f703fb251217fbca6bd0b640612f8d4414c91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "glossary/_filter_form.html.twig"));

        $__internal_615cb41f4096c0bfb4a5a5a8710fff2ef1a602a57cc07175e753dd5871ba1bec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_615cb41f4096c0bfb4a5a5a8710fff2ef1a602a57cc07175e753dd5871ba1bec->enter($__internal_615cb41f4096c0bfb4a5a5a8710fff2ef1a602a57cc07175e753dd5871ba1bec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "glossary/_filter_form.html.twig"));

        // line 8
        echo "<div class=\"panel panel-default\">
<div class=\"panel-body\">
";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("method" => "POST", "action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("glossary_index")));
        echo "
    <div class=\"col-sm-3\">
        ";
        // line 12
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "locale", array()), 'widget');
        echo "
    </div>
    <div class=\"col-sm-3\">
        ";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "term", array()), 'widget');
        echo "
    </div>
    <div class=\"col-sm-3\">
        <input type=\"submit\" value=\"Filter\" class=\"";
        // line 18
        echo twig_escape_filter($this->env, ((array_key_exists("button_css", $context)) ? (_twig_default_filter(($context["button_css"] ?? $this->getContext($context, "button_css")), "btn btn-primary")) : ("btn btn-primary")), "html", null, true);
        echo "\" />
    </div>
";
        // line 20
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
</div>
</div>";
        
        $__internal_524397b57fbed584a9955871e01f703fb251217fbca6bd0b640612f8d4414c91->leave($__internal_524397b57fbed584a9955871e01f703fb251217fbca6bd0b640612f8d4414c91_prof);

        
        $__internal_615cb41f4096c0bfb4a5a5a8710fff2ef1a602a57cc07175e753dd5871ba1bec->leave($__internal_615cb41f4096c0bfb4a5a5a8710fff2ef1a602a57cc07175e753dd5871ba1bec_prof);

    }

    public function getTemplateName()
    {
        return "glossary/_filter_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 20,  46 => 18,  40 => 15,  34 => 12,  29 => 10,  25 => 8,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
    By default, forms enable client-side validation. This means that you can't
    test the server-side validation errors from the browser. To temporarily
    disable this validation, add the 'novalidate' attribute:

    {{ form_start(form, {method: ..., action: ..., attr: {novalidate: 'novalidate'}}) }}
#}
<div class=\"panel panel-default\">
<div class=\"panel-body\">
{{ form_start(form, {method: 'POST', action: path('glossary_index')}) }}
    <div class=\"col-sm-3\">
        {{ form_widget(form.locale) }}
    </div>
    <div class=\"col-sm-3\">
        {{ form_widget(form.term) }}
    </div>
    <div class=\"col-sm-3\">
        <input type=\"submit\" value=\"Filter\" class=\"{{ button_css|default(\"btn btn-primary\") }}\" />
    </div>
{{ form_end(form) }}
</div>
</div>", "glossary/_filter_form.html.twig", "/var/www/proz/app/Resources/views/glossary/_filter_form.html.twig");
    }
}
