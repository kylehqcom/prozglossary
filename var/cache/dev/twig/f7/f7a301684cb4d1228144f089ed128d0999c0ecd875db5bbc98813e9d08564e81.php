<?php

/* security/login.html.twig */
class __TwigTemplate_6c578eccde6ef21f6fe0823d77534b9d2b80bd3145d108f67546678046f6ac3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "security/login.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_47fdd35dacc73fd7421a48ec26e3b109f56e7787d7d082d0f8b7cb671175b4d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_47fdd35dacc73fd7421a48ec26e3b109f56e7787d7d082d0f8b7cb671175b4d4->enter($__internal_47fdd35dacc73fd7421a48ec26e3b109f56e7787d7d082d0f8b7cb671175b4d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $__internal_b6d25ee039ea19bc8c3bf6a0d33eef8a26d87dc76b2de6e0edfe46dae965e573 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b6d25ee039ea19bc8c3bf6a0d33eef8a26d87dc76b2de6e0edfe46dae965e573->enter($__internal_b6d25ee039ea19bc8c3bf6a0d33eef8a26d87dc76b2de6e0edfe46dae965e573_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_47fdd35dacc73fd7421a48ec26e3b109f56e7787d7d082d0f8b7cb671175b4d4->leave($__internal_47fdd35dacc73fd7421a48ec26e3b109f56e7787d7d082d0f8b7cb671175b4d4_prof);

        
        $__internal_b6d25ee039ea19bc8c3bf6a0d33eef8a26d87dc76b2de6e0edfe46dae965e573->leave($__internal_b6d25ee039ea19bc8c3bf6a0d33eef8a26d87dc76b2de6e0edfe46dae965e573_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_5a7e62eb0b5f0befa3b6d55b775aace3746c2e93218a88c29bda5fc005532c42 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a7e62eb0b5f0befa3b6d55b775aace3746c2e93218a88c29bda5fc005532c42->enter($__internal_5a7e62eb0b5f0befa3b6d55b775aace3746c2e93218a88c29bda5fc005532c42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_9c7d0b3a56c47fe20f6757f335dbeb93d8e7263fccf3cbc11b4418b0f4d950e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c7d0b3a56c47fe20f6757f335dbeb93d8e7263fccf3cbc11b4418b0f4d950e1->enter($__internal_9c7d0b3a56c47fe20f6757f335dbeb93d8e7263fccf3cbc11b4418b0f4d950e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "login";
        
        $__internal_9c7d0b3a56c47fe20f6757f335dbeb93d8e7263fccf3cbc11b4418b0f4d950e1->leave($__internal_9c7d0b3a56c47fe20f6757f335dbeb93d8e7263fccf3cbc11b4418b0f4d950e1_prof);

        
        $__internal_5a7e62eb0b5f0befa3b6d55b775aace3746c2e93218a88c29bda5fc005532c42->leave($__internal_5a7e62eb0b5f0befa3b6d55b775aace3746c2e93218a88c29bda5fc005532c42_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_222e47a747f969cd8f1d2b3ea5c239edab05207161e2e6092c238caa1aa96dc4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_222e47a747f969cd8f1d2b3ea5c239edab05207161e2e6092c238caa1aa96dc4->enter($__internal_222e47a747f969cd8f1d2b3ea5c239edab05207161e2e6092c238caa1aa96dc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_5ce19a866860759f3e42b9cf253a19655d4aa0b26a278a201944a5bbc72d45e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ce19a866860759f3e42b9cf253a19655d4aa0b26a278a201944a5bbc72d45e1->enter($__internal_5ce19a866860759f3e42b9cf253a19655d4aa0b26a278a201944a5bbc72d45e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    ";
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 7
            echo "        <div class=\"alert alert-danger\">
            ";
            // line 8
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "
        </div>
    ";
        }
        // line 11
        echo "
    <div class=\"row\">
        <div class=\"col-sm-5\">
            <div class=\"well\">
                <form action=\"";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_login");
        echo "\" method=\"post\">
                    <fieldset>
                        <legend><i class=\"fa fa-lock\" aria-hidden=\"true\"></i> ";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title.login"), "html", null, true);
        echo "</legend>
                        <div class=\"form-group\">
                            <label for=\"username\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.username"), "html", null, true);
        echo "</label>
                            <input type=\"text\" id=\"username\" name=\"_username\" value=\"kyle\" class=\"form-control\"/>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"password\">";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.password"), "html", null, true);
        echo "</label>
                            <input type=\"password\" id=\"password\" name=\"_password\" value=\"password\" class=\"form-control\" />
                        </div>
                        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\"/>
                        <button type=\"submit\" class=\"btn btn-primary\">
                            <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i> ";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action.sign_in"), "html", null, true);
        echo "
                        </button>
                    </fieldset>
                </form>
            </div>
        </div>

        <div id=\"login-help\" class=\"col-sm-7\">
            <h3>
                <i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i>
                Try the following user
            </h3>

            <table class=\"table table-striped table-bordered table-hover\">
                <thead>
                    <tr>
                        <th scope=\"col\">";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.username"), "html", null, true);
        echo "</th>
                        <th scope=\"col\">";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.password"), "html", null, true);
        echo "</th>
                        <th scope=\"col\">";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.role"), "html", null, true);
        echo "</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>kyle</td>
                        <td>password</td>
                        <td><code>ROLE_ADMIN</code> (";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.role_admin"), "html", null, true);
        echo ")</td>
                    </tr>
                </tbody>
            </table>

            <div id=\"login-users-help\" class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <p>
                        <span class=\"label label-success\">";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("note"), "html", null, true);
        echo "</span>
                        ";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.reload_fixtures"), "html", null, true);
        echo "<br/>

                        <code class=\"console\">\$ php bin/console doctrine:fixtures:load</code>
                    </p>

                    <p>
                        <span class=\"label label-success\">";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("tip"), "html", null, true);
        echo "</span>
                        ";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.add_user"), "html", null, true);
        echo "<br/>

                        <code class=\"console\">\$ php bin/console app:add-user</code>
                    </p>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_5ce19a866860759f3e42b9cf253a19655d4aa0b26a278a201944a5bbc72d45e1->leave($__internal_5ce19a866860759f3e42b9cf253a19655d4aa0b26a278a201944a5bbc72d45e1_prof);

        
        $__internal_222e47a747f969cd8f1d2b3ea5c239edab05207161e2e6092c238caa1aa96dc4->leave($__internal_222e47a747f969cd8f1d2b3ea5c239edab05207161e2e6092c238caa1aa96dc4_prof);

    }

    // line 79
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_c690509cf4d9cb008dffd86b6fb39752c37a1f0c8764008189a7ad80c21f70fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c690509cf4d9cb008dffd86b6fb39752c37a1f0c8764008189a7ad80c21f70fe->enter($__internal_c690509cf4d9cb008dffd86b6fb39752c37a1f0c8764008189a7ad80c21f70fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_0aa8df15027f3bb03c8f9bdeb77e83e182b6ed1adce652e0c4b400b80afd34b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0aa8df15027f3bb03c8f9bdeb77e83e182b6ed1adce652e0c4b400b80afd34b8->enter($__internal_0aa8df15027f3bb03c8f9bdeb77e83e182b6ed1adce652e0c4b400b80afd34b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 80
        echo "    ";
        $this->displayParentBlock("sidebar", $context, $blocks);
        echo "

    ";
        // line 82
        echo $this->env->getExtension('CodeExplorerBundle\Twig\SourceCodeExtension')->showSourceCode($this->env, $this);
        echo "
";
        
        $__internal_0aa8df15027f3bb03c8f9bdeb77e83e182b6ed1adce652e0c4b400b80afd34b8->leave($__internal_0aa8df15027f3bb03c8f9bdeb77e83e182b6ed1adce652e0c4b400b80afd34b8_prof);

        
        $__internal_c690509cf4d9cb008dffd86b6fb39752c37a1f0c8764008189a7ad80c21f70fe->leave($__internal_c690509cf4d9cb008dffd86b6fb39752c37a1f0c8764008189a7ad80c21f70fe_prof);

    }

    // line 85
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_8b13748853f5cb9e63ae8834caf463d5912d4b5983ee6cc085f3b4a6402d5e92 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8b13748853f5cb9e63ae8834caf463d5912d4b5983ee6cc085f3b4a6402d5e92->enter($__internal_8b13748853f5cb9e63ae8834caf463d5912d4b5983ee6cc085f3b4a6402d5e92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_718a884519e654799bd609530c78a891e5d40a4e801a017fc975ada91b1ac288 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_718a884519e654799bd609530c78a891e5d40a4e801a017fc975ada91b1ac288->enter($__internal_718a884519e654799bd609530c78a891e5d40a4e801a017fc975ada91b1ac288_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 86
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

    <script>
        \$(document).ready(function() {
            var usernameEl = \$('#username');
            var passwordEl = \$('#password');
            // in a real application, hardcoding the user/password would be idiotic
            // but for the demo application it's very convenient to do so
            if (!usernameEl.val() && !passwordEl.val()) {
                usernameEl.val('jane_admin');
                passwordEl.val('kitten');
            }
        });
    </script>
";
        
        $__internal_718a884519e654799bd609530c78a891e5d40a4e801a017fc975ada91b1ac288->leave($__internal_718a884519e654799bd609530c78a891e5d40a4e801a017fc975ada91b1ac288_prof);

        
        $__internal_8b13748853f5cb9e63ae8834caf463d5912d4b5983ee6cc085f3b4a6402d5e92->leave($__internal_8b13748853f5cb9e63ae8834caf463d5912d4b5983ee6cc085f3b4a6402d5e92_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 86,  227 => 85,  215 => 82,  209 => 80,  200 => 79,  181 => 69,  177 => 68,  168 => 62,  164 => 61,  153 => 53,  143 => 46,  139 => 45,  135 => 44,  116 => 28,  111 => 26,  105 => 23,  98 => 19,  93 => 17,  88 => 15,  82 => 11,  76 => 8,  73 => 7,  70 => 6,  61 => 5,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'login' %}

{% block main %}
    {% if error %}
        <div class=\"alert alert-danger\">
            {{ error.messageKey|trans(error.messageData, 'security') }}
        </div>
    {% endif %}

    <div class=\"row\">
        <div class=\"col-sm-5\">
            <div class=\"well\">
                <form action=\"{{ path('security_login') }}\" method=\"post\">
                    <fieldset>
                        <legend><i class=\"fa fa-lock\" aria-hidden=\"true\"></i> {{ 'title.login'|trans }}</legend>
                        <div class=\"form-group\">
                            <label for=\"username\">{{ 'label.username'|trans }}</label>
                            <input type=\"text\" id=\"username\" name=\"_username\" value=\"kyle\" class=\"form-control\"/>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"password\">{{ 'label.password'|trans }}</label>
                            <input type=\"password\" id=\"password\" name=\"_password\" value=\"password\" class=\"form-control\" />
                        </div>
                        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token('authenticate') }}\"/>
                        <button type=\"submit\" class=\"btn btn-primary\">
                            <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i> {{ 'action.sign_in'|trans }}
                        </button>
                    </fieldset>
                </form>
            </div>
        </div>

        <div id=\"login-help\" class=\"col-sm-7\">
            <h3>
                <i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i>
                Try the following user
            </h3>

            <table class=\"table table-striped table-bordered table-hover\">
                <thead>
                    <tr>
                        <th scope=\"col\">{{ 'label.username'|trans }}</th>
                        <th scope=\"col\">{{ 'label.password'|trans }}</th>
                        <th scope=\"col\">{{ 'label.role'|trans }}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>kyle</td>
                        <td>password</td>
                        <td><code>ROLE_ADMIN</code> ({{ 'help.role_admin'|trans }})</td>
                    </tr>
                </tbody>
            </table>

            <div id=\"login-users-help\" class=\"panel panel-default\">
                <div class=\"panel-body\">
                    <p>
                        <span class=\"label label-success\">{{ 'note'|trans }}</span>
                        {{ 'help.reload_fixtures'|trans }}<br/>

                        <code class=\"console\">\$ php bin/console doctrine:fixtures:load</code>
                    </p>

                    <p>
                        <span class=\"label label-success\">{{ 'tip'|trans }}</span>
                        {{ 'help.add_user'|trans }}<br/>

                        <code class=\"console\">\$ php bin/console app:add-user</code>
                    </p>
                </div>
            </div>
        </div>
    </div>
{% endblock %}

{% block sidebar %}
    {{ parent() }}

    {{ show_source_code(_self) }}
{% endblock %}

{% block javascripts %}
    {{ parent() }}

    <script>
        \$(document).ready(function() {
            var usernameEl = \$('#username');
            var passwordEl = \$('#password');
            // in a real application, hardcoding the user/password would be idiotic
            // but for the demo application it's very convenient to do so
            if (!usernameEl.val() && !passwordEl.val()) {
                usernameEl.val('jane_admin');
                passwordEl.val('kitten');
            }
        });
    </script>
{% endblock %}
", "security/login.html.twig", "/var/www/proz/app/Resources/views/security/login.html.twig");
    }
}
