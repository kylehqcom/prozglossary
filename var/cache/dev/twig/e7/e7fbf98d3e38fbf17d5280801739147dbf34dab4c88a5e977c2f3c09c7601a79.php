<?php

/* admin/term/new.html.twig */
class __TwigTemplate_0d0971bfbd2b8d42dcbf066c7abee19d374e89109f93f03576a7fbd1f6443d0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/term/new.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d95790334a4fc769f336f223fb5d172fa938645b9f24739f1d11c12710a3277 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d95790334a4fc769f336f223fb5d172fa938645b9f24739f1d11c12710a3277->enter($__internal_5d95790334a4fc769f336f223fb5d172fa938645b9f24739f1d11c12710a3277_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/term/new.html.twig"));

        $__internal_c44c379ce76f6c7d87587256ccaeaee0bf0bda6e3b4f1f0f2a6553ff415d3828 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c44c379ce76f6c7d87587256ccaeaee0bf0bda6e3b4f1f0f2a6553ff415d3828->enter($__internal_c44c379ce76f6c7d87587256ccaeaee0bf0bda6e3b4f1f0f2a6553ff415d3828_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/term/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5d95790334a4fc769f336f223fb5d172fa938645b9f24739f1d11c12710a3277->leave($__internal_5d95790334a4fc769f336f223fb5d172fa938645b9f24739f1d11c12710a3277_prof);

        
        $__internal_c44c379ce76f6c7d87587256ccaeaee0bf0bda6e3b4f1f0f2a6553ff415d3828->leave($__internal_c44c379ce76f6c7d87587256ccaeaee0bf0bda6e3b4f1f0f2a6553ff415d3828_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_89c2cfadb144b0f33244bfa8f7073290bee245ce9e2f33baaa3867874ccce025 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_89c2cfadb144b0f33244bfa8f7073290bee245ce9e2f33baaa3867874ccce025->enter($__internal_89c2cfadb144b0f33244bfa8f7073290bee245ce9e2f33baaa3867874ccce025_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_c85bb4b8129f4954059b58adf2d1aa281f8838695260ad59a5a804e34e2b7898 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c85bb4b8129f4954059b58adf2d1aa281f8838695260ad59a5a804e34e2b7898->enter($__internal_c85bb4b8129f4954059b58adf2d1aa281f8838695260ad59a5a804e34e2b7898_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "admin_term_new";
        
        $__internal_c85bb4b8129f4954059b58adf2d1aa281f8838695260ad59a5a804e34e2b7898->leave($__internal_c85bb4b8129f4954059b58adf2d1aa281f8838695260ad59a5a804e34e2b7898_prof);

        
        $__internal_89c2cfadb144b0f33244bfa8f7073290bee245ce9e2f33baaa3867874ccce025->leave($__internal_89c2cfadb144b0f33244bfa8f7073290bee245ce9e2f33baaa3867874ccce025_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_15a85055dda1f11a914b5f145b1e5963d75d3684220e32607d0e55a351b37503 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15a85055dda1f11a914b5f145b1e5963d75d3684220e32607d0e55a351b37503->enter($__internal_15a85055dda1f11a914b5f145b1e5963d75d3684220e32607d0e55a351b37503_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_fdb4a461e519cd35c7505ff190daa6868ae30340ef7d68a407f8f6c2bc770841 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fdb4a461e519cd35c7505ff190daa6868ae30340ef7d68a407f8f6c2bc770841->enter($__internal_fdb4a461e519cd35c7505ff190daa6868ae30340ef7d68a407f8f6c2bc770841_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    <h1>Term creation</h1>
    ";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 9
        echo "        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "
    
        ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "locale", array()), 'row');
        echo "
        ";
        // line 12
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "term", array()), 'row');
        echo "
        ";
        // line 13
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "definition", array()), 'row');
        echo "
        <input type=\"submit\" value=\"Create Term\" class=\"btn btn-primary\" />
        <a href=\"";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("glossary_index");
        echo "\" class=\"btn btn-link\">
            Back to Glossary List
        </a>
    ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_fdb4a461e519cd35c7505ff190daa6868ae30340ef7d68a407f8f6c2bc770841->leave($__internal_fdb4a461e519cd35c7505ff190daa6868ae30340ef7d68a407f8f6c2bc770841_prof);

        
        $__internal_15a85055dda1f11a914b5f145b1e5963d75d3684220e32607d0e55a351b37503->leave($__internal_15a85055dda1f11a914b5f145b1e5963d75d3684220e32607d0e55a351b37503_prof);

    }

    // line 21
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_ad57852663d9a1d26d7d828393ed835792fc2d0d72c2aff781a0e2c7093e14de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad57852663d9a1d26d7d828393ed835792fc2d0d72c2aff781a0e2c7093e14de->enter($__internal_ad57852663d9a1d26d7d828393ed835792fc2d0d72c2aff781a0e2c7093e14de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_1f8a8404f78458f69d7e61bbca8d475134883bafb2c2f0394c6f85ee6e9ff6f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f8a8404f78458f69d7e61bbca8d475134883bafb2c2f0394c6f85ee6e9ff6f8->enter($__internal_1f8a8404f78458f69d7e61bbca8d475134883bafb2c2f0394c6f85ee6e9ff6f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 22
        echo "    ";
        $this->displayParentBlock("sidebar", $context, $blocks);
        echo "

    ";
        // line 24
        echo $this->env->getExtension('CodeExplorerBundle\Twig\SourceCodeExtension')->showSourceCode($this->env, $this);
        echo "
";
        
        $__internal_1f8a8404f78458f69d7e61bbca8d475134883bafb2c2f0394c6f85ee6e9ff6f8->leave($__internal_1f8a8404f78458f69d7e61bbca8d475134883bafb2c2f0394c6f85ee6e9ff6f8_prof);

        
        $__internal_ad57852663d9a1d26d7d828393ed835792fc2d0d72c2aff781a0e2c7093e14de->leave($__internal_ad57852663d9a1d26d7d828393ed835792fc2d0d72c2aff781a0e2c7093e14de_prof);

    }

    public function getTemplateName()
    {
        return "admin/term/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 24,  122 => 22,  113 => 21,  101 => 18,  95 => 15,  90 => 13,  86 => 12,  82 => 11,  76 => 9,  72 => 7,  69 => 6,  60 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'admin/layout.html.twig' %}

{% block body_id 'admin_term_new' %}

{% block main %}
    <h1>Term creation</h1>
    {{ form_start(form) }}
        {# Render any global form error (e.g. when a constraint on a public getter method failed) #}
        {{ form_errors(form) }}
    
        {{ form_row(form.locale) }}
        {{ form_row(form.term) }}
        {{ form_row(form.definition) }}
        <input type=\"submit\" value=\"Create Term\" class=\"btn btn-primary\" />
        <a href=\"{{ path('glossary_index') }}\" class=\"btn btn-link\">
            Back to Glossary List
        </a>
    {{ form_end(form) }}
{% endblock %}

{% block sidebar %}
    {{ parent() }}

    {{ show_source_code(_self) }}
{% endblock %}
", "admin/term/new.html.twig", "/var/www/proz/app/Resources/views/admin/term/new.html.twig");
    }
}
