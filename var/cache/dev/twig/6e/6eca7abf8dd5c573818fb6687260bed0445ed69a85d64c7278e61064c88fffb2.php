<?php

/* default/_source.html.twig */
class __TwigTemplate_ecef7c9aa0397438dd6db2353d0fb58a6a636e0a6cb7b0c7d68e3ebdfe77f4dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f77befeb1ce54f268deca558245c6c114abe65609993f9ec20d2a4fec5b2588f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f77befeb1ce54f268deca558245c6c114abe65609993f9ec20d2a4fec5b2588f->enter($__internal_f77befeb1ce54f268deca558245c6c114abe65609993f9ec20d2a4fec5b2588f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/_source.html.twig"));

        $__internal_958450a72d00bd09206ef00d21561dc8fbace743a79b1145d48b99d978af807e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_958450a72d00bd09206ef00d21561dc8fbace743a79b1145d48b99d978af807e->enter($__internal_958450a72d00bd09206ef00d21561dc8fbace743a79b1145d48b99d978af807e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/_source.html.twig"));

        // line 1
        if (($context["source"] ?? $this->getContext($context, "source"))) {
            // line 2
            echo "<div class=\"source default\">
    <h5>Glossary Default Source</h5>
    <p>";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute(($context["source"] ?? $this->getContext($context, "source")), "content", array()), "html", null, true);
            echo "</p>
</div>
";
        }
        
        $__internal_f77befeb1ce54f268deca558245c6c114abe65609993f9ec20d2a4fec5b2588f->leave($__internal_f77befeb1ce54f268deca558245c6c114abe65609993f9ec20d2a4fec5b2588f_prof);

        
        $__internal_958450a72d00bd09206ef00d21561dc8fbace743a79b1145d48b99d978af807e->leave($__internal_958450a72d00bd09206ef00d21561dc8fbace743a79b1145d48b99d978af807e_prof);

    }

    public function getTemplateName()
    {
        return "default/_source.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if source %}
<div class=\"source default\">
    <h5>Glossary Default Source</h5>
    <p>{{ source.content }}</p>
</div>
{% endif %}
", "default/_source.html.twig", "/var/www/proz/app/Resources/views/default/_source.html.twig");
    }
}
