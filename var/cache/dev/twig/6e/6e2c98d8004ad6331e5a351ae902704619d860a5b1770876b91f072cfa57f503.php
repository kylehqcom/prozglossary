<?php

/* form/layout.html.twig */
class __TwigTemplate_2a4aea16b92ac449db04ec1181895b7e04c9abec15f14b9ae58ec06add7fda8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("bootstrap_3_layout.html.twig", "form/layout.html.twig", 1);
        $this->blocks = array(
            'form_errors' => array($this, 'block_form_errors'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "bootstrap_3_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c85ffda57d4db79d4bfc655a2f6209ae1e6833ffc8bbdd28163e0a89b12ed1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8c85ffda57d4db79d4bfc655a2f6209ae1e6833ffc8bbdd28163e0a89b12ed1f->enter($__internal_8c85ffda57d4db79d4bfc655a2f6209ae1e6833ffc8bbdd28163e0a89b12ed1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/layout.html.twig"));

        $__internal_a2e26f3e8947be7e9ca5284cf14a236113301961bf244aa40528514f9ce291b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2e26f3e8947be7e9ca5284cf14a236113301961bf244aa40528514f9ce291b7->enter($__internal_a2e26f3e8947be7e9ca5284cf14a236113301961bf244aa40528514f9ce291b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8c85ffda57d4db79d4bfc655a2f6209ae1e6833ffc8bbdd28163e0a89b12ed1f->leave($__internal_8c85ffda57d4db79d4bfc655a2f6209ae1e6833ffc8bbdd28163e0a89b12ed1f_prof);

        
        $__internal_a2e26f3e8947be7e9ca5284cf14a236113301961bf244aa40528514f9ce291b7->leave($__internal_a2e26f3e8947be7e9ca5284cf14a236113301961bf244aa40528514f9ce291b7_prof);

    }

    // line 5
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_548c25f72de801edd92b1d9f94abac9eaec353c3899e882b3b9212ba4a40cfd7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_548c25f72de801edd92b1d9f94abac9eaec353c3899e882b3b9212ba4a40cfd7->enter($__internal_548c25f72de801edd92b1d9f94abac9eaec353c3899e882b3b9212ba4a40cfd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_d7cdbfc0bb4a7eeea2894b30ec6a10fc631693c1c3301bfd8ad6fecdf0ffcd41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7cdbfc0bb4a7eeea2894b30ec6a10fc631693c1c3301bfd8ad6fecdf0ffcd41->enter($__internal_d7cdbfc0bb4a7eeea2894b30ec6a10fc631693c1c3301bfd8ad6fecdf0ffcd41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 6
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 7
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 8
            echo "        <ul class=\"list-unstyled\">";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 11
                echo "            <li><span class=\"fa fa-exclamation-triangle\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "</ul>
        ";
            // line 14
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_d7cdbfc0bb4a7eeea2894b30ec6a10fc631693c1c3301bfd8ad6fecdf0ffcd41->leave($__internal_d7cdbfc0bb4a7eeea2894b30ec6a10fc631693c1c3301bfd8ad6fecdf0ffcd41_prof);

        
        $__internal_548c25f72de801edd92b1d9f94abac9eaec353c3899e882b3b9212ba4a40cfd7->leave($__internal_548c25f72de801edd92b1d9f94abac9eaec353c3899e882b3b9212ba4a40cfd7_prof);

    }

    public function getTemplateName()
    {
        return "form/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 14,  71 => 13,  63 => 11,  59 => 9,  57 => 8,  51 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'bootstrap_3_layout.html.twig' %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
        {% if form.parent %}<span class=\"help-block\">{% else %}<div class=\"alert alert-danger\">{% endif %}
        <ul class=\"list-unstyled\">
        {%- for error in errors -%}
            {# use font-awesome icon library #}
            <li><span class=\"fa fa-exclamation-triangle\"></span> {{ error.message }}</li>
        {%- endfor -%}
        </ul>
        {% if form.parent %}</span>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "form/layout.html.twig", "/var/www/proz/app/Resources/views/form/layout.html.twig");
    }
}
