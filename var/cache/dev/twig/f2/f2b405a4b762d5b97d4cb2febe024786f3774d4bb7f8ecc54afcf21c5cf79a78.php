<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_1ad8d5b1e6a4203f92b4fcf632db8ff29dccc727cc30f98dc9cab2a55a6e6883 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1b2aa4f144cd0273d655cfb82240998f9b03733cf734ab3dbcff316b55801961 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1b2aa4f144cd0273d655cfb82240998f9b03733cf734ab3dbcff316b55801961->enter($__internal_1b2aa4f144cd0273d655cfb82240998f9b03733cf734ab3dbcff316b55801961_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        $__internal_5a3cde7599514a9fb042278a336687e017c7c8a9ffc21b65a7d3df137fc4f027 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a3cde7599514a9fb042278a336687e017c7c8a9ffc21b65a7d3df137fc4f027->enter($__internal_5a3cde7599514a9fb042278a336687e017c7c8a9ffc21b65a7d3df137fc4f027_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_1b2aa4f144cd0273d655cfb82240998f9b03733cf734ab3dbcff316b55801961->leave($__internal_1b2aa4f144cd0273d655cfb82240998f9b03733cf734ab3dbcff316b55801961_prof);

        
        $__internal_5a3cde7599514a9fb042278a336687e017c7c8a9ffc21b65a7d3df137fc4f027->leave($__internal_5a3cde7599514a9fb042278a336687e017c7c8a9ffc21b65a7d3df137fc4f027_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
", "@Security/Collector/icon.svg", "/var/www/proz/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/views/Collector/icon.svg");
    }
}
