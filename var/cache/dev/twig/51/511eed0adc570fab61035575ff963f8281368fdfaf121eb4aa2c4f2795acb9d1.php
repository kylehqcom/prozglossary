<?php

/* admin/term/_delete_term_confirmation.html.twig */
class __TwigTemplate_126842724030d3b48f097a5f01e9d9038545b512838bb39544e59d115c80ef6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d56e3a594fdf6c99955738c95a20c79ae936f0456f9575803194b058c828a2ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d56e3a594fdf6c99955738c95a20c79ae936f0456f9575803194b058c828a2ee->enter($__internal_d56e3a594fdf6c99955738c95a20c79ae936f0456f9575803194b058c828a2ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/term/_delete_term_confirmation.html.twig"));

        $__internal_444f5635699239dd10003a9f35cba3fd15467a780df6506bb44fca596cdb06e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_444f5635699239dd10003a9f35cba3fd15467a780df6506bb44fca596cdb06e3->enter($__internal_444f5635699239dd10003a9f35cba3fd15467a780df6506bb44fca596cdb06e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/term/_delete_term_confirmation.html.twig"));

        // line 2
        echo "<div class=\"modal fade\" id=\"confirmationModal\" tabindex=\"-1\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-body\">
                <h4>Are you sure you want to delete this term?</h4>
                <p>";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("delete_post_modal.body"), "html", null, true);
        echo "</p>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" id=\"btnNo\" data-dismiss=\"modal\">
                    ";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.cancel"), "html", null, true);
        echo "
                </button>
                <button type=\"button\" class=\"btn btn-danger\" id=\"btnYes\" data-dismiss=\"modal\">
                    Delete term
                </button>
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_d56e3a594fdf6c99955738c95a20c79ae936f0456f9575803194b058c828a2ee->leave($__internal_d56e3a594fdf6c99955738c95a20c79ae936f0456f9575803194b058c828a2ee_prof);

        
        $__internal_444f5635699239dd10003a9f35cba3fd15467a780df6506bb44fca596cdb06e3->leave($__internal_444f5635699239dd10003a9f35cba3fd15467a780df6506bb44fca596cdb06e3_prof);

    }

    public function getTemplateName()
    {
        return "admin/term/_delete_term_confirmation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 11,  32 => 7,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# Bootstrap modal, see http://getbootstrap.com/javascript/#modals #}
<div class=\"modal fade\" id=\"confirmationModal\" tabindex=\"-1\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-body\">
                <h4>Are you sure you want to delete this term?</h4>
                <p>{{ 'delete_post_modal.body'|trans }}</p>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\" id=\"btnNo\" data-dismiss=\"modal\">
                    {{ 'label.cancel'|trans }}
                </button>
                <button type=\"button\" class=\"btn btn-danger\" id=\"btnYes\" data-dismiss=\"modal\">
                    Delete term
                </button>
            </div>
        </div>
    </div>
</div>
", "admin/term/_delete_term_confirmation.html.twig", "/var/www/proz/app/Resources/views/admin/term/_delete_term_confirmation.html.twig");
    }
}
