<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_e93bf973098f7cf635c270b2305797c4c15b364e4aeba788e67f45333d1d8223 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a6b382453c79c375aafe39a81b438803307505fe5f4aeb871b47fc1902710427 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6b382453c79c375aafe39a81b438803307505fe5f4aeb871b47fc1902710427->enter($__internal_a6b382453c79c375aafe39a81b438803307505fe5f4aeb871b47fc1902710427_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_1998ae56cac9bb0a332fc72e837a5608735209aeeb976412915e9f5cb33e6498 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1998ae56cac9bb0a332fc72e837a5608735209aeeb976412915e9f5cb33e6498->enter($__internal_1998ae56cac9bb0a332fc72e837a5608735209aeeb976412915e9f5cb33e6498_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a6b382453c79c375aafe39a81b438803307505fe5f4aeb871b47fc1902710427->leave($__internal_a6b382453c79c375aafe39a81b438803307505fe5f4aeb871b47fc1902710427_prof);

        
        $__internal_1998ae56cac9bb0a332fc72e837a5608735209aeeb976412915e9f5cb33e6498->leave($__internal_1998ae56cac9bb0a332fc72e837a5608735209aeeb976412915e9f5cb33e6498_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_17be77552b8ece557570f68957a806026a9a2e112fcf44bbaca41ecee77c8018 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17be77552b8ece557570f68957a806026a9a2e112fcf44bbaca41ecee77c8018->enter($__internal_17be77552b8ece557570f68957a806026a9a2e112fcf44bbaca41ecee77c8018_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_79640aeac87e804c2e99eb70875b36125b30bba2295d4180f2821eeab93bbe87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_79640aeac87e804c2e99eb70875b36125b30bba2295d4180f2821eeab93bbe87->enter($__internal_79640aeac87e804c2e99eb70875b36125b30bba2295d4180f2821eeab93bbe87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_79640aeac87e804c2e99eb70875b36125b30bba2295d4180f2821eeab93bbe87->leave($__internal_79640aeac87e804c2e99eb70875b36125b30bba2295d4180f2821eeab93bbe87_prof);

        
        $__internal_17be77552b8ece557570f68957a806026a9a2e112fcf44bbaca41ecee77c8018->leave($__internal_17be77552b8ece557570f68957a806026a9a2e112fcf44bbaca41ecee77c8018_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_6b0a79662ed24dae78cba13e69b257b21139c53456c5488ac1377a0c148edb47 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b0a79662ed24dae78cba13e69b257b21139c53456c5488ac1377a0c148edb47->enter($__internal_6b0a79662ed24dae78cba13e69b257b21139c53456c5488ac1377a0c148edb47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_de402b007cb6d1be3f1a3148791d6a510444a13947bccdc65acacde34edf5dff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de402b007cb6d1be3f1a3148791d6a510444a13947bccdc65acacde34edf5dff->enter($__internal_de402b007cb6d1be3f1a3148791d6a510444a13947bccdc65acacde34edf5dff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_de402b007cb6d1be3f1a3148791d6a510444a13947bccdc65acacde34edf5dff->leave($__internal_de402b007cb6d1be3f1a3148791d6a510444a13947bccdc65acacde34edf5dff_prof);

        
        $__internal_6b0a79662ed24dae78cba13e69b257b21139c53456c5488ac1377a0c148edb47->leave($__internal_6b0a79662ed24dae78cba13e69b257b21139c53456c5488ac1377a0c148edb47_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_61c0832e8146ceb4fa41c196f831b63319a54b9b3a572a2d5614a0a400cf1156 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_61c0832e8146ceb4fa41c196f831b63319a54b9b3a572a2d5614a0a400cf1156->enter($__internal_61c0832e8146ceb4fa41c196f831b63319a54b9b3a572a2d5614a0a400cf1156_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_2b975730492d8c61d1469d4f6ad03c048cb416e470b3e1ad90809ddd7f328ec5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2b975730492d8c61d1469d4f6ad03c048cb416e470b3e1ad90809ddd7f328ec5->enter($__internal_2b975730492d8c61d1469d4f6ad03c048cb416e470b3e1ad90809ddd7f328ec5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_2b975730492d8c61d1469d4f6ad03c048cb416e470b3e1ad90809ddd7f328ec5->leave($__internal_2b975730492d8c61d1469d4f6ad03c048cb416e470b3e1ad90809ddd7f328ec5_prof);

        
        $__internal_61c0832e8146ceb4fa41c196f831b63319a54b9b3a572a2d5614a0a400cf1156->leave($__internal_61c0832e8146ceb4fa41c196f831b63319a54b9b3a572a2d5614a0a400cf1156_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/var/www/proz/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
