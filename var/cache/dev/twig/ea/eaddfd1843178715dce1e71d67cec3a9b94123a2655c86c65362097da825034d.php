<?php

/* admin/term/_form.html.twig */
class __TwigTemplate_2f7ec31f22339bcb2b0fa594d5a7a9bfb49e59382461c94590b0a74aadaf550f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_243556df035faf54525484a0d0914c100389e11ffd9d7a3840d7051ecbf308ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_243556df035faf54525484a0d0914c100389e11ffd9d7a3840d7051ecbf308ba->enter($__internal_243556df035faf54525484a0d0914c100389e11ffd9d7a3840d7051ecbf308ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/term/_form.html.twig"));

        $__internal_1fa32904d4b2b3171b26f6ebb12993d34c1f2b9f84b3f7ae0d2c245b48510efa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1fa32904d4b2b3171b26f6ebb12993d34c1f2b9f84b3f7ae0d2c245b48510efa->enter($__internal_1fa32904d4b2b3171b26f6ebb12993d34c1f2b9f84b3f7ae0d2c245b48510efa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/term/_form.html.twig"));

        // line 8
        echo "
";
        // line 9
        if (((array_key_exists("show_confirmation", $context)) ? (_twig_default_filter(($context["show_confirmation"] ?? $this->getContext($context, "show_confirmation")), false)) : (false))) {
            // line 10
            echo "    ";
            $context["attr"] = array("data-confirmation" => "true");
            // line 11
            echo "    ";
            echo twig_include($this->env, $context, "glossary/_delete_term_confirmation.html.twig");
            echo "
";
        }
        // line 13
        echo "
";
        // line 14
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("attr" => ((array_key_exists("attr", $context)) ? (_twig_default_filter(($context["attr"] ?? $this->getContext($context, "attr")), array())) : (array()))));
        echo "
    ";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "

    <input type=\"submit\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, ((array_key_exists("button_label", $context)) ? (_twig_default_filter(($context["button_label"] ?? $this->getContext($context, "button_label")), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.create_post"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.create_post"))), "html", null, true);
        echo "\"
           class=\"";
        // line 18
        echo twig_escape_filter($this->env, ((array_key_exists("button_css", $context)) ? (_twig_default_filter(($context["button_css"] ?? $this->getContext($context, "button_css")), "btn btn-primary")) : ("btn btn-primary")), "html", null, true);
        echo "\" />

    ";
        // line 20
        if (((array_key_exists("include_back_to_home_link", $context)) ? (_twig_default_filter(($context["include_back_to_home_link"] ?? $this->getContext($context, "include_back_to_home_link")), false)) : (false))) {
            // line 21
            echo "        <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("glossary_index");
            echo "\" class=\"btn btn-link\">
            Back to the glossary list
        </a>
    ";
        }
        // line 25
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_243556df035faf54525484a0d0914c100389e11ffd9d7a3840d7051ecbf308ba->leave($__internal_243556df035faf54525484a0d0914c100389e11ffd9d7a3840d7051ecbf308ba_prof);

        
        $__internal_1fa32904d4b2b3171b26f6ebb12993d34c1f2b9f84b3f7ae0d2c245b48510efa->leave($__internal_1fa32904d4b2b3171b26f6ebb12993d34c1f2b9f84b3f7ae0d2c245b48510efa_prof);

    }

    public function getTemplateName()
    {
        return "admin/term/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 25,  62 => 21,  60 => 20,  55 => 18,  51 => 17,  46 => 15,  42 => 14,  39 => 13,  33 => 11,  30 => 10,  28 => 9,  25 => 8,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
    By default, forms enable client-side validation. This means that you can't
    test the server-side validation errors from the browser. To temporarily
    disable this validation, add the 'novalidate' attribute:

    {{ form_start(form, {attr: {novalidate: 'novalidate'}}) }}
#}

{% if show_confirmation|default(false) %}
    {% set attr = {'data-confirmation': 'true'} %}
    {{ include('glossary/_delete_term_confirmation.html.twig') }}
{% endif %}

{{ form_start(form, {attr: attr|default({})}) }}
    {{ form_widget(form) }}

    <input type=\"submit\" value=\"{{ button_label|default('label.create_post'|trans) }}\"
           class=\"{{ button_css|default(\"btn btn-primary\") }}\" />

    {% if include_back_to_home_link|default(false) %}
        <a href=\"{{ path('glossary_index') }}\" class=\"btn btn-link\">
            Back to the glossary list
        </a>
    {% endif %}
{{ form_end(form) }}
", "admin/term/_form.html.twig", "/var/www/proz/app/Resources/views/admin/term/_form.html.twig");
    }
}
