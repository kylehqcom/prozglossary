<?php

/* admin/term/edit.html.twig */
class __TwigTemplate_040984b3876603b3cf336752c5d259eb1e3d530718d5ef907b0263e8b8a08c1d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("admin/layout.html.twig", "admin/term/edit.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fb71420eee6bec58db9a126f7a357bcb6abb27ebf7528554b4f4e64a5d3e14f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb71420eee6bec58db9a126f7a357bcb6abb27ebf7528554b4f4e64a5d3e14f4->enter($__internal_fb71420eee6bec58db9a126f7a357bcb6abb27ebf7528554b4f4e64a5d3e14f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/term/edit.html.twig"));

        $__internal_a060a0ab94bd6379f449a6df9ac3a826edd0c8a7c24e9d1a6e4fdbab89269926 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a060a0ab94bd6379f449a6df9ac3a826edd0c8a7c24e9d1a6e4fdbab89269926->enter($__internal_a060a0ab94bd6379f449a6df9ac3a826edd0c8a7c24e9d1a6e4fdbab89269926_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/term/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fb71420eee6bec58db9a126f7a357bcb6abb27ebf7528554b4f4e64a5d3e14f4->leave($__internal_fb71420eee6bec58db9a126f7a357bcb6abb27ebf7528554b4f4e64a5d3e14f4_prof);

        
        $__internal_a060a0ab94bd6379f449a6df9ac3a826edd0c8a7c24e9d1a6e4fdbab89269926->leave($__internal_a060a0ab94bd6379f449a6df9ac3a826edd0c8a7c24e9d1a6e4fdbab89269926_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_224805c3be41ce30a90bd94ca641806f75ba4ff6c58d9c6901f16a93b30e6f48 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_224805c3be41ce30a90bd94ca641806f75ba4ff6c58d9c6901f16a93b30e6f48->enter($__internal_224805c3be41ce30a90bd94ca641806f75ba4ff6c58d9c6901f16a93b30e6f48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_fbf1fe4761b742622dddf5bcc81c0eebc88156a70dc6055975894fbf76066c4d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fbf1fe4761b742622dddf5bcc81c0eebc88156a70dc6055975894fbf76066c4d->enter($__internal_fbf1fe4761b742622dddf5bcc81c0eebc88156a70dc6055975894fbf76066c4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "admin_term_edit";
        
        $__internal_fbf1fe4761b742622dddf5bcc81c0eebc88156a70dc6055975894fbf76066c4d->leave($__internal_fbf1fe4761b742622dddf5bcc81c0eebc88156a70dc6055975894fbf76066c4d_prof);

        
        $__internal_224805c3be41ce30a90bd94ca641806f75ba4ff6c58d9c6901f16a93b30e6f48->leave($__internal_224805c3be41ce30a90bd94ca641806f75ba4ff6c58d9c6901f16a93b30e6f48_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_3ee3fa7f5130fd6e42d00db825992aa9207ef7eeeea694f858d338b63a5b146f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3ee3fa7f5130fd6e42d00db825992aa9207ef7eeeea694f858d338b63a5b146f->enter($__internal_3ee3fa7f5130fd6e42d00db825992aa9207ef7eeeea694f858d338b63a5b146f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_85517d7f1aa603fbeb3e66572547bc8e8129a0831119685d3725e6b1e47e471e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_85517d7f1aa603fbeb3e66572547bc8e8129a0831119685d3725e6b1e47e471e->enter($__internal_85517d7f1aa603fbeb3e66572547bc8e8129a0831119685d3725e6b1e47e471e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    <h1>Edit: ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["term"] ?? $this->getContext($context, "term")), "term", array()), "html", null, true);
        echo "</h1>

    ";
        // line 8
        echo twig_include($this->env, $context, "admin/term/_form.html.twig", array("form" =>         // line 9
($context["form"] ?? $this->getContext($context, "form")), "button_label" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action.save"), "include_back_to_home_link" => true), false);
        // line 12
        echo "
";
        
        $__internal_85517d7f1aa603fbeb3e66572547bc8e8129a0831119685d3725e6b1e47e471e->leave($__internal_85517d7f1aa603fbeb3e66572547bc8e8129a0831119685d3725e6b1e47e471e_prof);

        
        $__internal_3ee3fa7f5130fd6e42d00db825992aa9207ef7eeeea694f858d338b63a5b146f->leave($__internal_3ee3fa7f5130fd6e42d00db825992aa9207ef7eeeea694f858d338b63a5b146f_prof);

    }

    // line 15
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_29248b82de75bfab3a173603d574013563083092c18faf373ab481accae6fe26 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29248b82de75bfab3a173603d574013563083092c18faf373ab481accae6fe26->enter($__internal_29248b82de75bfab3a173603d574013563083092c18faf373ab481accae6fe26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_6efde81cf4ecddda02df41efe8b6c5f9d8c339a77633cb5d6c11e7a0440f13ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6efde81cf4ecddda02df41efe8b6c5f9d8c339a77633cb5d6c11e7a0440f13ea->enter($__internal_6efde81cf4ecddda02df41efe8b6c5f9d8c339a77633cb5d6c11e7a0440f13ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 16
        echo "    <div class=\"section actions\">
        ";
        // line 17
        echo twig_include($this->env, $context, "admin/term/_delete_form.html.twig", array("term" => ($context["term"] ?? $this->getContext($context, "term"))), false);
        echo "
    </div>

    ";
        // line 20
        $this->displayParentBlock("sidebar", $context, $blocks);
        echo "

    ";
        // line 22
        echo $this->env->getExtension('CodeExplorerBundle\Twig\SourceCodeExtension')->showSourceCode($this->env, $this);
        echo "
";
        
        $__internal_6efde81cf4ecddda02df41efe8b6c5f9d8c339a77633cb5d6c11e7a0440f13ea->leave($__internal_6efde81cf4ecddda02df41efe8b6c5f9d8c339a77633cb5d6c11e7a0440f13ea_prof);

        
        $__internal_29248b82de75bfab3a173603d574013563083092c18faf373ab481accae6fe26->leave($__internal_29248b82de75bfab3a173603d574013563083092c18faf373ab481accae6fe26_prof);

    }

    public function getTemplateName()
    {
        return "admin/term/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 22,  107 => 20,  101 => 17,  98 => 16,  89 => 15,  78 => 12,  76 => 9,  75 => 8,  69 => 6,  60 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'admin/layout.html.twig' %}

{% block body_id 'admin_term_edit' %}

{% block main %}
    <h1>Edit: {{ term.term }}</h1>

    {{ include('admin/term/_form.html.twig', {
        form: form,
        button_label: 'action.save'|trans,
        include_back_to_home_link: true,
    }, with_context = false) }}
{% endblock %}

{% block sidebar %}
    <div class=\"section actions\">
        {{ include('admin/term/_delete_form.html.twig', {term: term}, with_context = false) }}
    </div>

    {{ parent() }}

    {{ show_source_code(_self) }}
{% endblock %}
", "admin/term/edit.html.twig", "/var/www/proz/app/Resources/views/admin/term/edit.html.twig");
    }
}
