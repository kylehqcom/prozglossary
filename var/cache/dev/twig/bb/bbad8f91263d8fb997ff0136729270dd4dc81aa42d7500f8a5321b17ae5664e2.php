<?php

/* form/fields.html.twig */
class __TwigTemplate_84df40bdfa1e11f608f9fc25ddbd9a0d175e67b1dceb60b462b7e866f67b0c62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'date_time_picker_widget' => array($this, 'block_date_time_picker_widget'),
            'tags_input_widget' => array($this, 'block_tags_input_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d6b80c12de6b7d30692bb61f06a6039f34312805b7201133dd6104ea45ded454 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d6b80c12de6b7d30692bb61f06a6039f34312805b7201133dd6104ea45ded454->enter($__internal_d6b80c12de6b7d30692bb61f06a6039f34312805b7201133dd6104ea45ded454_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/fields.html.twig"));

        $__internal_24cbb910944a00dd18d787becddcc7760f824ea87df8d70f576a23a6cb3b78a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24cbb910944a00dd18d787becddcc7760f824ea87df8d70f576a23a6cb3b78a1->enter($__internal_24cbb910944a00dd18d787becddcc7760f824ea87df8d70f576a23a6cb3b78a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/fields.html.twig"));

        // line 9
        echo "
";
        // line 10
        $this->displayBlock('date_time_picker_widget', $context, $blocks);
        // line 18
        echo "
";
        // line 19
        $this->displayBlock('tags_input_widget', $context, $blocks);
        
        $__internal_d6b80c12de6b7d30692bb61f06a6039f34312805b7201133dd6104ea45ded454->leave($__internal_d6b80c12de6b7d30692bb61f06a6039f34312805b7201133dd6104ea45ded454_prof);

        
        $__internal_24cbb910944a00dd18d787becddcc7760f824ea87df8d70f576a23a6cb3b78a1->leave($__internal_24cbb910944a00dd18d787becddcc7760f824ea87df8d70f576a23a6cb3b78a1_prof);

    }

    // line 10
    public function block_date_time_picker_widget($context, array $blocks = array())
    {
        $__internal_f717f4d4c7d9f09127661bc858795d99134fdb3b489f23249d4dccf51b31224a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f717f4d4c7d9f09127661bc858795d99134fdb3b489f23249d4dccf51b31224a->enter($__internal_f717f4d4c7d9f09127661bc858795d99134fdb3b489f23249d4dccf51b31224a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_time_picker_widget"));

        $__internal_3f4cac5890062a9bd6346f7adb1c34a2e6e9857c756b28a237667a88cc7dcb1a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f4cac5890062a9bd6346f7adb1c34a2e6e9857c756b28a237667a88cc7dcb1a->enter($__internal_3f4cac5890062a9bd6346f7adb1c34a2e6e9857c756b28a237667a88cc7dcb1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_time_picker_widget"));

        // line 11
        echo "    <div class=\"input-group date\" data-toggle=\"datetimepicker\">
        ";
        // line 12
        $this->displayBlock("datetime_widget", $context, $blocks);
        echo "
        <span class=\"input-group-addon\">
            <span class=\"fa fa-calendar\" aria-hidden=\"true\"></span>
        </span>
    </div>
";
        
        $__internal_3f4cac5890062a9bd6346f7adb1c34a2e6e9857c756b28a237667a88cc7dcb1a->leave($__internal_3f4cac5890062a9bd6346f7adb1c34a2e6e9857c756b28a237667a88cc7dcb1a_prof);

        
        $__internal_f717f4d4c7d9f09127661bc858795d99134fdb3b489f23249d4dccf51b31224a->leave($__internal_f717f4d4c7d9f09127661bc858795d99134fdb3b489f23249d4dccf51b31224a_prof);

    }

    // line 19
    public function block_tags_input_widget($context, array $blocks = array())
    {
        $__internal_a339e5a554ecf4a0b3bcba4b5bf0ceb2468806e88e9bd956dca7316b2722cd8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a339e5a554ecf4a0b3bcba4b5bf0ceb2468806e88e9bd956dca7316b2722cd8c->enter($__internal_a339e5a554ecf4a0b3bcba4b5bf0ceb2468806e88e9bd956dca7316b2722cd8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        $__internal_8984aa464ed24def20577c2f33a3953ef70babf1b846fde5b6953fb4e2524169 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8984aa464ed24def20577c2f33a3953ef70babf1b846fde5b6953fb4e2524169->enter($__internal_8984aa464ed24def20577c2f33a3953ef70babf1b846fde5b6953fb4e2524169_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        // line 20
        echo "    <div class=\"input-group\">
        ";
        // line 21
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget', array("attr" => array("data-toggle" => "tagsinput", "data-tags" => twig_jsonencode_filter(($context["tags"] ?? $this->getContext($context, "tags"))))));
        echo "
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
";
        
        $__internal_8984aa464ed24def20577c2f33a3953ef70babf1b846fde5b6953fb4e2524169->leave($__internal_8984aa464ed24def20577c2f33a3953ef70babf1b846fde5b6953fb4e2524169_prof);

        
        $__internal_a339e5a554ecf4a0b3bcba4b5bf0ceb2468806e88e9bd956dca7316b2722cd8c->leave($__internal_a339e5a554ecf4a0b3bcba4b5bf0ceb2468806e88e9bd956dca7316b2722cd8c_prof);

    }

    public function getTemplateName()
    {
        return "form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 21,  82 => 20,  73 => 19,  57 => 12,  54 => 11,  45 => 10,  35 => 19,  32 => 18,  30 => 10,  27 => 9,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
   Each field type is rendered by a template fragment, which is determined
   by the name of your form type class (DateTimePickerType -> date_time_picker)
   and the suffix \"_widget\". This can be controlled by overriding getBlockPrefix()
   in DateTimePickerType.

   See https://symfony.com/doc/current/cookbook/form/create_custom_field_type.html#creating-a-template-for-the-field
#}

{% block date_time_picker_widget %}
    <div class=\"input-group date\" data-toggle=\"datetimepicker\">
        {{ block('datetime_widget') }}
        <span class=\"input-group-addon\">
            <span class=\"fa fa-calendar\" aria-hidden=\"true\"></span>
        </span>
    </div>
{% endblock %}

{% block tags_input_widget %}
    <div class=\"input-group\">
        {{ form_widget(form, {'attr': {'data-toggle': 'tagsinput', 'data-tags': tags|json_encode}}) }}
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
{% endblock %}
", "form/fields.html.twig", "/var/www/proz/app/Resources/views/form/fields.html.twig");
    }
}
