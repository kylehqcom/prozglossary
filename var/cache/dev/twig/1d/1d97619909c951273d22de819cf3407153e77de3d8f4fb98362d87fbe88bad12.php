<?php

/* admin/term/_delete_form.html.twig */
class __TwigTemplate_0f424e7ad2929b464a0dd4e1de3e76790f5b2d727b883d8807ba0a358fe05802 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f456b9b57be50da5b9b8635ecbd844c0ee1b46bf37c696ac6d78a6a8712223d6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f456b9b57be50da5b9b8635ecbd844c0ee1b46bf37c696ac6d78a6a8712223d6->enter($__internal_f456b9b57be50da5b9b8635ecbd844c0ee1b46bf37c696ac6d78a6a8712223d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/term/_delete_form.html.twig"));

        $__internal_1ed244daaea6567eebad14cdb0f727dbadd0c5bf2a23a4a2b07e406ab825dd44 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ed244daaea6567eebad14cdb0f727dbadd0c5bf2a23a4a2b07e406ab825dd44->enter($__internal_1ed244daaea6567eebad14cdb0f727dbadd0c5bf2a23a4a2b07e406ab825dd44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/term/_delete_form.html.twig"));

        // line 1
        echo twig_include($this->env, $context, "admin/term/_delete_term_confirmation.html.twig");
        echo "
<form action=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl("admin_term_delete", array("id" => $this->getAttribute(($context["term"] ?? $this->getContext($context, "term")), "id", array()))), "html", null, true);
        echo "\" method=\"post\" data-confirmation=\"true\" id=\"delete-form\">
    <input type=\"hidden\" name=\"token\" value=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderCsrfToken("delete"), "html", null, true);
        echo "\" />
    <button type=\"submit\" class=\"btn btn-lg btn-block btn-danger\">
        <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>
        Delete term
    </button>
</form>
";
        
        $__internal_f456b9b57be50da5b9b8635ecbd844c0ee1b46bf37c696ac6d78a6a8712223d6->leave($__internal_f456b9b57be50da5b9b8635ecbd844c0ee1b46bf37c696ac6d78a6a8712223d6_prof);

        
        $__internal_1ed244daaea6567eebad14cdb0f727dbadd0c5bf2a23a4a2b07e406ab825dd44->leave($__internal_1ed244daaea6567eebad14cdb0f727dbadd0c5bf2a23a4a2b07e406ab825dd44_prof);

    }

    public function getTemplateName()
    {
        return "admin/term/_delete_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 3,  29 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('admin/term/_delete_term_confirmation.html.twig') }}
<form action=\"{{ url('admin_term_delete', {id: term.id}) }}\" method=\"post\" data-confirmation=\"true\" id=\"delete-form\">
    <input type=\"hidden\" name=\"token\" value=\"{{ csrf_token('delete') }}\" />
    <button type=\"submit\" class=\"btn btn-lg btn-block btn-danger\">
        <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>
        Delete term
    </button>
</form>
", "admin/term/_delete_form.html.twig", "/var/www/proz/app/Resources/views/admin/term/_delete_form.html.twig");
    }
}
