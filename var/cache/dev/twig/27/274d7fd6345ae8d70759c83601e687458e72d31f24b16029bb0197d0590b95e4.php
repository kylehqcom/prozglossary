<?php

/* admin/layout.html.twig */
class __TwigTemplate_953fd8af6fd516fd7c4c3a774520e6a59869fa81cad6b84ac490b943d262967e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 8
        $this->parent = $this->loadTemplate("base.html.twig", "admin/layout.html.twig", 8);
        $this->blocks = array(
            'header_navigation_links' => array($this, 'block_header_navigation_links'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ed2afd188d9cb5032f4a52102219c756118d659b562ed45735cc392f315ff9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ed2afd188d9cb5032f4a52102219c756118d659b562ed45735cc392f315ff9d->enter($__internal_9ed2afd188d9cb5032f4a52102219c756118d659b562ed45735cc392f315ff9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/layout.html.twig"));

        $__internal_b8068b212d31174b728a5744c24314ff28f8e5befc5083d8c7439b2d620c1265 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8068b212d31174b728a5744c24314ff28f8e5befc5083d8c7439b2d620c1265->enter($__internal_b8068b212d31174b728a5744c24314ff28f8e5befc5083d8c7439b2d620c1265_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9ed2afd188d9cb5032f4a52102219c756118d659b562ed45735cc392f315ff9d->leave($__internal_9ed2afd188d9cb5032f4a52102219c756118d659b562ed45735cc392f315ff9d_prof);

        
        $__internal_b8068b212d31174b728a5744c24314ff28f8e5befc5083d8c7439b2d620c1265->leave($__internal_b8068b212d31174b728a5744c24314ff28f8e5befc5083d8c7439b2d620c1265_prof);

    }

    // line 10
    public function block_header_navigation_links($context, array $blocks = array())
    {
        $__internal_176aae8d006325622835014b4e5d9fcf9a8a267d8bc17f0433e5802167af0ccd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_176aae8d006325622835014b4e5d9fcf9a8a267d8bc17f0433e5802167af0ccd->enter($__internal_176aae8d006325622835014b4e5d9fcf9a8a267d8bc17f0433e5802167af0ccd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        $__internal_314d4a19e9a47e25f94a60fd508ffe3d0d6a5912b58c901e5040d5d416577155 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_314d4a19e9a47e25f94a60fd508ffe3d0d6a5912b58c901e5040d5d416577155->enter($__internal_314d4a19e9a47e25f94a60fd508ffe3d0d6a5912b58c901e5040d5d416577155_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        // line 11
        echo "    <li>
        <a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
            <i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i> Glossary List
        </a>
    </li>
";
        
        $__internal_314d4a19e9a47e25f94a60fd508ffe3d0d6a5912b58c901e5040d5d416577155->leave($__internal_314d4a19e9a47e25f94a60fd508ffe3d0d6a5912b58c901e5040d5d416577155_prof);

        
        $__internal_176aae8d006325622835014b4e5d9fcf9a8a267d8bc17f0433e5802167af0ccd->leave($__internal_176aae8d006325622835014b4e5d9fcf9a8a267d8bc17f0433e5802167af0ccd_prof);

    }

    public function getTemplateName()
    {
        return "admin/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 12,  49 => 11,  40 => 10,  11 => 8,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
   This is the base template of the all backend pages. Since this layout is similar
   to the global layout, we inherit from it to just change the contents of some
   blocks. In practice, backend templates are using a three-level inheritance,
   showing how powerful, yet easy to use, is Twig's inheritance mechanism.
   See https://symfony.com/doc/current/book/templating.html#template-inheritance-and-layouts
#}
{% extends 'base.html.twig' %}

{% block header_navigation_links %}
    <li>
        <a href=\"{{ path('homepage') }}\">
            <i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i> Glossary List
        </a>
    </li>
{% endblock %}
", "admin/layout.html.twig", "/var/www/proz/app/Resources/views/admin/layout.html.twig");
    }
}
